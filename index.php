<?


error_reporting(E_ALL);
ini_set('display_errors', 1);

error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR );

require_once($_SERVER['DOCUMENT_ROOT'] . '/php/config/defines.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/config/path.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/core/Autoloader.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/core/Auth.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/core/Core.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/core/Route.php');

//$core = Core::getCore();

//if($core->auth->is_auth){
    //echo '<!--Final: '.memory_get_usage().' bytes-->';
    //echo '<!--Peak: '. memory_get_peak_usage().' bytes-->';
//}

Autoloader::init();
core\Route::start();
//Mail::feedback('web-bOZKNJ@mail-tester.com');
