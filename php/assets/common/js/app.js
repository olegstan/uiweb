//var app = angular.module("app", [])
//    .controller("treeController", ["$scope", "$http", function ($scope, $http, $location){
//        $scope.list = [
//            {
//                name: 'node',
//                collapsed: false,
//                initialized: false,
//                hasChildren: true,
//                children: [
//                    {
//                        name: 'node',
//                        collapsed: false,
//                        initialized: false,
//                        hasChildren: true,
//                        children: [
//                            {
//
//                            }
//                        ]
//                    }
//                ]
//            },
//            {
//                name: 'node',
//                collapsed: false,
//                initialized: false,
//                hasChildren: true
//            },
//            {
//                name: 'node',
//                collapsed: false,
//                initialized: false,
//                hasChildren: true
//            }
//        ]
//    }])
//    .directive("ngTree", ["$compile", function ($compile) {
//        var directive = {};
//
//        directive.restrict = 'A';
//        directive.link = function(scope, element, attrs){
//
//        };
//        directive.controller = function ($scope) {
//
//        };
//        directive.scope = {
//            'datatata': '='
//        };
//        directive.template = '<ul>' +
//            '<li ng-transclude></li>' +
//            '<li ng-repeat="child in list.children">>' +
//                //'<div ng-tree data="child">{{child.name}}</div>' +
//            '</li>' +
//        '</ul>';
//        directive.transclude = true;
//        directive.compile = function(tElement, tAttrs, transclude){
//            //console.log(scope);
//            var contents = tElement.contents().remove();
//            var compiledContents;
//            return function(scope, iElement, iAttr) {
//                if(!compiledContents) {
//                    compiledContents = $compile(contents, transclude);
//                }
//                compiledContents(scope, function(clone, scope) {
//                    iElement.append(clone);
//                });
//            };
//
//            /*return {
//                pre: function(scope, iElement, iAttrs, controller) {
//
//                },
//                post: function(scope, iElement, iAttrs, controller) {
//
//
//                    console.log(scope);
//                    //console.log(tAttrs);
//                    //console.log(transclude);
//                }
//            }*/
//        };
//        return directive;
//    }]);
var module = angular.module('app', []);

module.controller("treeController", ["$scope", "$http", function($scope, $http) {
    $scope.nodes = [
        {
            id: 1,
            name: "node1",
            collapsed: true,
            initialized: false,
            hasChildren: false,
            children: [
                {
                    id: 2,
                    name: "node2",
                    collapsed: false,
                    initialized: false,
                    hasChildren: true,
                    children: [
                        {
                            id: 3,
                            name: "node3",
                            collapsed: false,
                            initialized: false,
                            hasChildren: true
                        }
                    ]
                }
            ]
        },
        {
            id: 2,
            name: "node2",
            collapsed: false,
            initialized: false,
            hasChildren: false
        }
    ];

    $scope.updateData = function(){
        $http.get('/ajax/test')
            .success(function (data, status, headers, confi) {
                $scope.nodes = data;
            })
            .error(function (data, status, headers, confi) {

            });
    }
}]);

module.directive("ngTree", ["$compile", function($compile) {
    return {
        restrict: "A",
        //transclude: true,
        scope: {
            ngTree: '='
        },
        template:
            '<ul ng-repeat="element in ngTree">' +
                '<li>{{element.name}}</li>' +
                '<button ng-click="toggleCollapse(element)">+</button>' +
                '<div ng-show="element.children && element.collapsed" ng-tree="element.children" data-id="{{element.name}}">{{element.name}}</div>' +
            '</ul>',
        link: function (scope, element, attrs) {

        },
        controller: function ($scope) {
            $scope.toggleCollapse = function (element) {
                element.collapsed = !element.collapsed;
            };
        },
        compile: function(element, attrs, transclude) {
            var contents = element.contents().remove();
            var compiledContents;
            return function(scope, element, attrs) {
                if(!compiledContents) {
                    compiledContents = $compile(contents, transclude);
                }
                compiledContents(scope, function(clone) {
                    element.append(clone);
                });
            };
        }
    };
}]);