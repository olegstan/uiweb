﻿Russia (Россия)||+7
Afghanistan (‫افغانستان‬‎)||+93
Albania (Shqipëri)||+355
Algeria||+213
American Samoa||+1684
Andorra||+376
Angola||+244
Anguilla||+1264
Antigua Barbuda||+1268
Argentina||+54
Armenia (Հայաստան)||+374
Aruba||+297
Ascension Island||+247
Australia||+61
Austria (Österreich)||+43
Azerbaijan (Azərbaycan)||+994
Bahamas||+1242
Bahrain (‫البحرين‬‎)||+973
Bangladesh (বাংলাদেশ)||+880
Barbados||+1246
Belarus (Беларусь)||+375
Belgium (België)||+32
Belize||+501
Benin (Bénin)||+229
Bermuda||+1441
Bhutan (འབྲུག)||+975
Bolivia||+591
Bosnia &amp; Herzegovina (Босна и Херцеговина)||+387
Botswana||+267
Brazil (Brasil)||+55
British Indian Ocean Territory||+246
British Virgin Islands||+1284
Brunei||+673
Bulgaria (България)||+359
Burkina Faso||+226
Burundi (Uburundi)||+257
Cambodia (កម្ពុជា)||+855
Cameroon (Cameroun)||+237
Canada||+1
Cape Verde (Kabu Verdi)||+238
Caribbean Netherlands||+599
Cayman Islands||+1345
Central African Republic (République centrafricaine)||+236
Chad (Tchad)||+235
Chile||+56
China (中国)||+86
Colombia||+57
Comoros (‫جزر القمر‬‎)||+269
Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)||+243
Congo (Republic) (Congo-Brazzaville)||+242
Cook Islands||+682
Costa Rica||+506
Côte d’Ivoire||+225
Croatia (Hrvatska)||+385
Cuba||+53
Curaçao||+599
Cyprus (Κύπρος)||+357
Czech Republic (Česká republika)||+420
Denmark (Danmark)||+45
Djibouti||+253
Dominica||+1767
Dominican Republic (República Dominicana)||+1809
Ecuador||+593
Egypt (‫مصر‬‎)||+20
El Salvador||+503
Equatorial Guinea (Guinea Ecuatorial)||+240
Eritrea||+291
Estonia (Eesti)||+372
Ethiopia||+251
Falkland Islands (Islas Malvinas)||+500
Faroe Islands (Føroyar)||+298
Fiji||+679
Finland (Suomi)||+358
France||+33
French Guiana (Guyane française)||+594
French Polynesia (Polynésie française)||+689
Gabon||+241
Gambia||+220
Georgia (საქართველო)||+995
Germany (Deutschland)||+49
Ghana (Gaana)||+233
Gibraltar||+350
Greece (Ελλάδα)||+30
Greenland (Kalaallit Nunaat)||+299
Grenada||+1473
Guadeloupe||+590
Guam||+1671
Guatemala||+502
Guinea (Guinée)||+224
Guinea-Bissau (Guiné Bissau)||+245
Guyana||+592
Haiti||+509
Honduras||+504
Hong Kong (香港)||+852
Hungary (Magyarország)||+36
Iceland (Ísland)||+354
India (भारत)||+91
Indonesia||+62
Iran (‫ایران‬‎)||+98
Iraq (‫العراق‬‎)||+964
Ireland||+353
Israel (‫ישראל‬‎)||+972
Italy (Italia)||+39
Jamaica||+1876
Japan (日本)||+81
Jordan (‫الأردن‬‎)||+962
Kazakhstan (Казахстан)||+7
Kenya||+254
Kiribati||+686
Kuwait (‫الكويت‬‎)||+965
Kyrgyzstan (Кыргызстан)||+996
Laos (ລາວ)||+856
Latvia (Latvija)||+371
Lebanon (‫لبنان‬‎)||+961
Lesotho||+266
Liberia||+231
Libya (‫ليبيا‬‎)||+218
Liechtenstein||+423
Lithuania (Lietuva)||+370
Luxembourg||+352
Macau (澳門)||+853
Macedonia (FYROM) (Македонија)||+389
Madagascar (Madagasikara)||+261
Malawi||+265
Malaysia||+60
Maldives||+960
Mali||+223
Malta||+356
Marshall Islands||+692
Martinique||+596
Mauritania (‫موريتانيا‬‎)||+222
Mauritius (Moris)||+230
Mexico (México)||+52
Micronesia||+691
Moldova (Republica Moldova)||+373
Monaco||+377
Mongolia (Монгол)||+976
Montenegro (Crna Gora)||+382
Montserrat||+1664
Morocco||+212
Mozambique (Moçambique)||+258
Myanmar (Burma) (မြန်မာ)||+95
Namibia (Namibië)||+264
Nauru||+674
Nepal (नेपाल)||+977
Netherlands (Nederland)||+31
New Caledonia (Nouvelle-Calédonie)||+687
New Zealand||+64
Nicaragua||+505
Niger (Nijar)||+227
Nigeria||+234
Niue||+683
Norfolk Island||+6723
Northern Mariana Islands||+1
North Korea (조선 민주주의 인민 공화국)||+850
Norway (Norge)||+47
Oman (‫عُمان‬‎)||+968
Pakistan (‫پاکستان‬‎)||+92
Palau||+680
Palestine (‫فلسطين‬‎)||+970
Panama (Panamá)||+507
Papua New Guinea||+675
Paraguay||+595
Peru (Perú)||+51
Philippines||+63
Poland (Polska)||+48
Portugal||+351
Puerto Rico||+1787
Qatar (‫قطر‬‎)||+974
Réunion (La Réunion)||+262
Romania (România)||+40
Russia (Россия)||+7
Rwanda||+250
Samoa||+685
San Marino||+378
São Tomé &amp; Príncipe (São Tomé e Príncipe)||+239
Saudi Arabia (‫المملكة العربية السعودية‬‎)||+966
Senegal||+221
Serbia (Србија)||+381
Seychelles||+248
Sierra Leone||+232
Singapore||+65
Sint Maarten||+1721
Slovakia (Slovensko)||+421
Slovenia (Slovenija)||+386
Solomon Islands||+677
Somalia (Soomaaliya)||+252
South Africa||+27
South Korea (대한민국)||+82
South Sudan (‫جنوب السودان‬‎)||+211
Spain (España)||+34
Sri Lanka (ශ්‍රී ලංකාව)||+94
St. Barthélemy (Saint-Barthélemy)||+590
St. Helena||+290
St. Kitts &amp; Nevis||+1869
St. Lucia||+1758
St. Martin (Saint-Martin (partie française))||+590
St. Pierre &amp; Miquelon (Saint-Pierre-et-Miquelon)||+508
St. Vincent &amp; Grenadines||+1784
Sudan (‫السودان‬‎)||+249
Suriname||+597
Swaziland||+268
Sweden (Sverige)||+46
Switzerland (Schweiz)||+41
Syria (‫سوريا‬‎)||+963
Taiwan (台灣)||+886
Tajikistan||+992
Tanzania||+255
Thailand (ไทย)||+66
Timor-Leste||+670
Togo||+228
Tokelau||+690
Tonga||+676
Trinidad &amp; Tobago||+1868
Tunisia||+216
Turkey (Türkiye)||+90
Turkmenistan||+993
Turks &amp; Caicos Islands||+1649
Tuvalu||+688
U.S. Virgin Islands||+1340
Uganda||+256
Ukraine (Україна)||+380
United Arab Emirates (‫الإمارات العربية المتحدة‬‎)||+971
United Kingdom||+44
United States||+1
Uruguay||+598
Uzbekistan (Oʻzbekiston)||+998
Vanuatu||+678
Vatican City (Città del Vaticano)||+379
Venezuela||+58
Vietnam (Việt Nam)||+84
Wallis Futuna||+681
Yemen (‫اليمن‬‎)||+967
Zambia||+260
Zimbabwe||+263
