<?
namespace core;

use core\helper\Flash;

class Core
{
	public $routes;
    public $language = 'ru';
    public $default_language = 'ru';
    public $controller = 'main';
    public $controller_name = 'Main';
    public $action = 'index';
    public $action_uri = 'index';
    public $request_id = null;

    public $view;
    public $asset;
    //data to layout
    public $data;

    public $languages = ['ru', 'en'];
    public $locales = ['ru_RU', 'en_US'];

    public $layout;
    public $default_layout;
	
	public $title;
	public $keywords;
	public $description;
	
	public $remote_ip;
	public $protocol;
	public $host;
    public $root;
	public $method;
	public $referer;
	public $session;
	public $cookie;

	/* singleton */
	static public $core;
	public $auth;
	public $flash;
    public $query;
    public $redis;

    public function __construct()
    {

    }

	public function init()
	{
        $this->auth = new Auth();
        $this->flash = new Flash();
        $this->asset = new AssetBundle();
		//$this->protocol = $_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http';
		$this->protocol = SERVER_PROTOCOL;
        $this->remote_ip =& $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
        $this->host =& $_SERVER['HTTP_HOST'];


		$this->root = $this->protocol . '://' . $this->host;
        //$this->ip =
		$request_uri =& $_SERVER['REQUEST_URI'];
		$this->method =& $_SERVER['REQUEST_METHOD'];
		$this->referer =& $_SERVER['HTTP_REFERER'];
        $this->session =& $_SESSION;
        $this->cookie =& $_COOKIE;

		$parts = explode('?', $request_uri);

		$this->routes = explode('/', $parts[0]);

		return $this;
	}
	
	public static function getCore()
    {
        if (static::$core === null) {
	        static::$core = new static();
        }
        return static::$core;
    }
	
}