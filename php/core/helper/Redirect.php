<?
namespace core\helper;

use core\Core;

class Redirect
{
    public function __construct()
    {
        $this->core = Core::getCore();
    }

    public function redirect($url = '')
    {
        header('Location:' . $this->core->root . $url);
        return $this;
    }

    public function auth()
    {
        header('Location:' . $this->core->root . '/user/login/');
        return $this;
    }
}