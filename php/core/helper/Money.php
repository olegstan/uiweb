<?
namespace core\helper;

class Money
{
    function money($amount, $symbol = '$')
    {
        return $symbol . money_format('%i', $amount);
    }

    // Examples (will vary depending on LC_MONETARY config):
    //echo money(1); // $1.00
    //echo money(1234); // $1234.00
    //echo money(1234.56,''); // 1234.56
    //echo money(1234.56,'£'); // £1234.56
    //- See more at: http://laravelsnippets.com/snippets/quick-money-formatting#sthash.8DWyP6iC.dpuf
}