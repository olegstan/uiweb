<?php
namespace core\helper;

class Hash
{
    private static $options = [
        'cost' => HASH_COST,
        'salt' => HASH_SALT
    ];

    public static function password($str)
    {
        return password_hash($str, PASSWORD_BCRYPT, self::$options);
    }

    public static function verify($str, $hash)
    {
        return password_verify($str, $hash);
    }

    public function random($length)
    {

    }

}