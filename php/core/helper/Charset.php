<?
namespace core\helper;

class Charset
{
    public static function is_utf8($string)
    {
        return (mb_detect_encoding($string, 'UTF-8', true) == 'UTF-8');
    }
}