<?
namespace core\helper;

// переписать на автоподгрузку моделей

//написать URL multicheckbox unique
class Validation
{
    /**
     * @var array
     */

    public $a_errors = [];
    public $a_success = [];

    public function validateInteger($value, $keyname, $error_message = 'Нужно ввести число', $success_message = '')
    {
        if(!filter_var($value, FILTER_VALIDATE_INT)){
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    public function validateString($value, $keyname, $error_message = '', $success_message = '')
    {
        if (!is_string($value)) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    /**
     * @param $value
     * @param $keyname
     * @param string $error_message
     */

    public function validateEmpty($value, $keyname, $error_message = 'Введите же что-нибудь', $success_message = '')
    {
        /**
         * может прийти null тогда просто возвращаем ошибку
         *
         * при цифре 0 написать проверку
         *
         */

        if(is_array($value) && empty($value)){
            $this->a_errors[$keyname] = $error_message;
        }else if(is_string($value) && trim(empty($value))) {
            $this->a_errors[$keyname] = $error_message;
        }else if(is_int($value) && empty($value)){
            $this->a_errors[$keyname] = $error_message;
        }else if(is_null($value)){
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    /**
     * @param $value
     * @param $keyname
     * @param $pattern
     * @param string $error_message
     */

    public function validatePattern($value, $keyname, $pattern, $error_message = 'Допускаются только символы латинского алфавита и цифры', $success_message = '')
    {
        if (!preg_match('#^' . $pattern . '$#', $value)) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    /**
     * @param $value
     * @param $keyname
     * @param $pattern
     * @param string $error_message
     */

    public function validateDynamicPattern($value, $keyname, $class_name, $class_id, $error_message = 'Допускаются только символы латинского алфавита и цифры', $success_message = '')
    {
        $class_name = 'app\\model\\' . $class_name;
        $model = new $class_name;
        $model = $model->query->select()->where('`' . $model->table . '`.`id` = :id', [':id' => $class_id])->execute()->one()->getResult();

        if (!preg_match('#^' . $model->pattern . '$#', $value)) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    public function validateMaxValue($value, $keyname, $max_value, $error_message = 'Максимальное значение', $success_message = '')
    {

        //if(!filter_var($value, FILTER_VALIDATE_INT)){
        //filter_var($int, FILTER_VALIDATE_INT, array("options" => array("min_range"=>$min, "max_range"=>$max))

        if (!is_numeric($value) && $value > $max_value) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    public function validateMinValue($value, $keyname, $min_value, $error_message = 'Минимальное значение', $success_message = '')
    {
        //filter_var($int, FILTER_VALIDATE_INT, array("options" => array("min_range"=>$min, "max_range"=>$max))


        if (!is_numeric($value) && $value < $min_value) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    public function validateBetween($value, $keyname, $min_value, $max_value, $error_message = 'Значение должно находиться в диапазоне', $success_message = '')
    {
        //filter_var($int, FILTER_VALIDATE_INT, array("options" => array("min_range"=>$min, "max_range"=>$max))


        if($min_value < $value && $value < $max_value){
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    /**
     * @param $value
     * @param $keyname
     * @param $length
     * @param string $error_message
     */

    public function validateMaxLength($value, $keyname, $length, $error_message = 'Не более 32 символов', $success_message = '')
    {
        if (Text::strlenUtf8(Text::toUtf8($value)) > $length) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    /**
     * @param $value
     * @param $keyname
     * @param $length
     * @param string $error_message
     */

    public function validateMinLength($value, $keyname, $length, $error_message = 'Не более 32 символов', $success_message = '')
    {
        /**
         * не самое быстрое решение
         */
        if (Text::strlenUtf8(Text::toUtf8($value)) > $length) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    /**
     * @param $value
     * @param $keyname
     * @param $field
     * @param bool $current_value
     * @param string $error_message
     */

    public function validateUniqueField($value, $keyname, $class_name, $class_field, $table_field, $error_message = 'Данное значение не уникально', $success_message = '', $current_value = false)
    {
        /**
         * переписать указывать название модели с namespace
         */
        $class_name = 'app\\model\\' . $class_name;
        $model = new $class_name();
        $model = $model->query->select()->where($table_field . ' = :' . $class_field, [':' . $class_field => $value])->execute()->one()->getResult();
        if ($model !== null) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    public function validateUniqueFields($values, $keyname, $class_name, $class_fields, $table_fields, $error_message = 'Данное значение не уникально', $success_message = '')
    {
        $class_name = 'app\\model\\' . $class_name;
        $model = new $class_name;

        $condition = '';
        $and = '';

        foreach ($class_fields as $k => $field) {
            if($condition){
                $and = ' AND ';
            }

            $condition .= $and . $table_fields[$k] . ' = :' . $field;
            $bind[':' . $field] = $values[$k];
        }

        $model = $model->query->select()->where($condition, $bind)->execute()->one()->getResult();

        if ($model !== null) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    /**
     * @param $value
     * @param $keyname
     * @param string $error_message
     */

    public function validateEmailPattern($value, $keyname, $error_message = 'Адрес электронной почты не соотвествует формату', $success_message = '')
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    /**
     * @param $value
     * @param $keyname
     * @param string $error_message
     */

    public function validateIP4Pattern($value, $keyname, $error_message = 'IP адрес не соответствует формату', $success_message = '')
    {
        if (!filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    /**
     * @param $value
     * @param $keyname
     * @param string $error_message
     */

    public function validateIP6Pattern($value, $keyname, $error_message = 'IP адрес не соответствует формату', $success_message = '')
    {
        if (!filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    /**
     * @param $value1
     * @param $value2
     * @param $keyname1
     * @param $keyname2
     * @param string $error_message
     */

    public function validateCompare($value1, $value2, $keyname1, $keyname2, $error_message = 'Введенные пароли не совпадают', $success_message = '')
    {
        if (strcmp($value1, $value2) !== 0) {
            $this->a_errors[$keyname1] = $error_message;
            $this->a_errors[$keyname2] = $error_message;
        }else{
            $this->a_success[$keyname1] = $success_message;
            $this->a_success[$keyname2] = $success_message;
        }
    }

    /**
     * @param $value
     * @param $keyname
     * @param string $error_message
     */

    function validateCheckbox($value, $keyname, $error_message = 'Необходимо ознакомиться с условиями оферты', $success_message = '')
    {
        if ($_REQUEST[$keyname] != $value) {
            $this->a_errors[$keyname] = $error_message;
        }else{
            $this->a_success[$keyname] = $success_message;
        }
    }

    //если self будет true, то будет проверка на сравнение с текущим именем пользователя
    //убрать повторяющиеся строки
    /*public function validateName($keyname, $current_name)
    {
        $name = strip_tags(trim($_REQUEST[$keyname]));

        $this->validateEmpty($name, $keyname);
        $this->validatePattern($name, $keyname, '#^[0-9a-zA-Z_ ]+$#', 'Не допускаются набранные символы');
        $this->validateLength($name, $keyname, 32);
        $this->validateUnique($name, $keyname, '_name', $current_name, 'Пользователь с таким логином уже зарегистрирован');
    }

    function validateEmail($keyname, $current_email)
    {
        $email = strip_tags(trim($_REQUEST[$keyname]));

        $this->validateUnique($email, $keyname, 'email', $current_email, 'Пользователь с такой электронной почтой уже существует');
        $this->validateLength($email, $keyname, 32);
        $this->validateEmailPattern($email, $keyname);
        $this->validateEmpty($email, $keyname);
    }*/

    /*function validatePhone($keyphone, $current_phone)
    {
        $phone = strip_tags(trim($_REQUEST[$keyphone]));

        $this->validateUnique($phone, $keyphone, 'phone_preferred', $current_phone, 'Пользователь с таким телефоном уже существует');
        $this->validatePattern($phone, $keyphone, '#^\+7\-[0-9]{3}\-[0-9]{3}\-[0-9]{2}\-[0-9]{2}$#', 'Телефон не соответствует формату. Пример: +7-964-789-01-25');
        $this->validateEmpty($phone, $keyphone);
    }*/

    /*function validatePassword($keyname1, $keyname2)
    {
        $password = strip_tags(trim($_REQUEST[$keyname1]));
        $password_repeat = strip_tags(trim($_REQUEST[$keyname2]));

        $this->validateEmpty($password, $keyname1, 'Пожалуйста, введите пароль');
        $this->validateCompare($password, $password_repeat, $keyname1, $keyname2);
    }*/


    /*function validateRegistration($keyname, $keyemail, $keypassword, $keypassword_repeat, $checkbox)
    {
        $this->validateName($keyname);
        $this->validateEmail($keyemail);
        $this->validatePassword($keypassword, $keypassword_repeat);
        //$this->validateCheckbox($checkbox);

        return $this->a_errors;
    }*/

    /*function validateUpdateProfile($keyname, $keyemail, $keyphone)
    {
        $this->validateName($keyname, $user->_name);
        $this->validateEmail($keyemail, $user->email);
        $this->validatePhone($keyphone, $user->phone_preferred);

        return $this->a_errors;
    }

    function validateEmailAndPhone($keyemail, $keyphone)
    {
        $this->validateEmail($keyemail, $user->email);
        $this->validatePhone($keyphone, $user->phone_preferred);

        return $this->a_errors;
    }*/

}
