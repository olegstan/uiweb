<?
namespace core\helper;

class File
{
    public function getFiles($path)
    {
        if ($handle = opendir($path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry !== '.' && $entry !== '..') {
                    $files[] = $entry;
                }
            }
            closedir($handle);
        }
        return $files;
    }

    /**
     * @param $path
     */

    public function read($path)
    {
        $handle = fopen($path, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $lines[] = $line;
            }
            fclose($handle);

            return $lines;
        } else {
            return;
        }
    }

    public function get($path)
    {

    }

    public function save($file_name, $path)
    {

    }

    public function saveAsString($string, $file_name, $path)
    {
        file_put_contents($path . $file_name, $string);
    }

    public function remove()
    {

    }

    public function download($path)
    {
        if (file_exists($path)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($path));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            readfile($path);
            die();
        }
    }

    public function upload()
    {

    }
}