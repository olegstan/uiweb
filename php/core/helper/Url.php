<?
namespace core\helper;

use core\Core;

class Url
{
    public $host;

    public function __construct()
    {
        $this->core = Core::getCore();
    }

    public function route($controller, $action = null, $id = null)
    {
        if($id != null){
            if($this->core->language == $this->core->default_language){
                return $this->core->root . '/' . $controller . '/' . $action . '/' . $id;
            }else{
                return $this->core->root . '/' . $this->core->language . '/' . $controller . '/' . $action . '/' . $id;
            }
        }else if($action != null){
            if($this->core->language == $this->core->default_language){
                return $this->core->root . '/' . $controller . '/' . $action;
            }else{
                return $this->core->root . '/' . $this->core->language . '/' . $controller . '/' . $action;
            }
        }else{
            if($this->core->language == $this->core->default_language){
                return $this->core->root . '/' . $controller . '/';
            }else{
                return $this->core->root . '/' . $this->core->language . '/' . $controller . '/';
            }
        }
    }

    public function home()
    {
        if ($this->core->language !== 'ru') {
            return $this->core->root . '/' . $this->core->language;
        } else {
            return $this->core->root;
        }
    }
}