<?
namespace core\helper;

use core\Core;
use core\View;

class Template
{
    public $result;

    public $start = '<?=';
    public $end = ';?>';



    public function __construct()
    {
        $this->core = Core::getCore();
        $this->view = new View();
        $this->core->view = $this->view;
    }

    public function parse()
    {
        $start = '<?=';
        $end = ';?>';

        //$template = str_replace('@component()', '$this->core->view->component', $template);
        $this->template = str_replace('@view', $start . '$this->core->view->view->renderView()' . $end,  $this->template);

        return $this->template;
    }

    public function render($cache = false)
    {

        if($cache){
            $path = ABS . '/php/tmp/cached_template/' . $this->core->controller . '-' . $this->core->layout . '-' . $this->core->language . '.tpl.php';

            if(file_exists($path)){
                require_once($path);
            }else{
                $template = $this->view->layout->render($this->core->layout);
                file_put_contents($path, $template);
                require_once($path);
            }
        }else{
            return $this->view->layout->render($this->core->layout);
        }
    }
}