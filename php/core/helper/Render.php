<?
namespace core\helper;

use core\Core;

class Render
{
    public $result;

    public function __construct($type)
    {
        $this->core = Core::getCore();
        $this->type = $type;
        $this->url = new Url();
    }

    public function render($view)
    {
        $language_file_path = ABS . '/php/app/' . $this->type . '/' . $view . '/lang/' . $this->core->language . '.php';
        $view_file_path = ABS . '/php/app/' . $this->type . '/' . $view . '/' . $view . '.php';

        return $this->logic($language_file_path, $view_file_path);
    }

    public function renderView()
    {
        $language_file_path = ABS . '/php/app/' . $this->type . '/' . $this->core->default_layout . '/' . $this->core->action_uri . '/lang/' . $this->core->language . '.php';
        $view_file_path = ABS . '/php/app/' . $this->type . '/' . $this->core->default_layout . '/' . $this->core->action_uri . '/' . $this->core->action_uri . '.php';

        return $this->logic($language_file_path, $view_file_path);
    }

    public function logic($language_file_path, $view_file_path)
    {
        ob_start();

        $t = $this->getTranslate($language_file_path);

        $this->result = $this->getView($view_file_path, $t);

        ob_end_clean();

        return $this->result;
    }

    public function getTranslate($path)
    {
        if (file_exists($path)) {
            return require($path);
        } else {
            die(__FILE__ . ' ' . __LINE__ . 'Не найден файл: ' . $path);
        }
    }

    public function getView($path, $t)
    {
        if ($this->core->data !== null) {
            extract($this->core->data);
        }

        if (file_exists($path)) {
            require($path);
        } else {
            die(__FILE__ . '' . __LINE__ . 'Не найден файл вида: ' . $path);
        }

        return ob_get_contents();
    }
}