<?
namespace core\helper;

class Header
{
    public static function json()
    {
        header("Cache-Control: no-cache, must-revalidate");
        header('Content-Type: application/json');
    }
}