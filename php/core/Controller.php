<?
namespace core;

use core\helper\Redirect;
use core\helper\Template;

abstract class Controller
{
    public function __construct()
    {
        $this->core = Core::getCore();
        $this->redirect = new Redirect();
        $this->template = new Template();
    }

    public function actionIndex()
    {

    }

    public function actionView($id)
    {

    }

    public function actionDelete($id)
    {

    }

    public function actionInsert()
    {

    }

    public function actionUpdate($id)
    {

    }

    public function render($data = null, $cache = false)
    {
        $this->core->data = $data;
        return $this->template->render($cache);
    }

    public function redirect($path = '')
    {
        $this->redirect->redirect($path);
    }
}