<?
namespace core;

use Iterator;

class Collection implements Iterator
{
    private $position = 0;
    private $items = [];

    public function __construct()
    {
        $this->position = 0;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->items[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->items[$this->position]);
    }

    public function appendToArray($element, $key = null)
    {
        if(isset($key)){
            $this->items[$key] = $element;
        }else{
            $this->items[] = $element;
        }
    }

    public function appendOne($element)
    {
        $this->items = $element;
    }

    public function getId()
    {
        if (is_array($this->items)) {
            $result_ids = [];
            foreach ($this->items as $k => $v) {
                $result_ids[] = $v->id;
            }
            $this->result_ids = $result_ids;
            return $this->result_ids;
        } else {
            return;
        }
    }

    public function getField($field)
    {
        if (is_array($this->items)) {
            $fields = [];
            foreach ($this->items as $k => $v) {
                $fields[] = $v->$field;
            }
            $this->result_fields = $fields;
            return $this->result_fields;
        } else {
            return;
        }
    }

    public function getResult()
    {
        if(!empty($this->items)){
            return $this->items;
        }else{
            return null;
        }
    }
}