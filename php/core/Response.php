<?
namespace core;

use core\helper\Header;

class Response
{
    public static function json($a_array = null)
    {
        Header::json();
        die(json_encode($a_array));
    }
}
