<?
namespace core;

use app\model\User;
use core\helper\Hash;

class Auth
{
    public $session;
    public $cookie;

    public function __construct()
    {
        $this->sessionStart();
        $this->session =& $_SESSION;
        $this->cookie =& $_COOKIE;
        $this->is_auth = $this->isAuth();
    }

    public function sessionStart()
    {
        //PHP_SESSION_DISABLED - 0 if sessions are disabled.
        //PHP_SESSION_NONE - 1 if sessions are enabled, but none exists.
        //PHP_SESSION_ACTIVE - 2 if sessions are enabled, and one exists.
		
        //проверяем существует ли сессия
        if(session_status() == PHP_SESSION_NONE) {
            //указываем директорию для сохранения сессий
            if (is_writable(ABS . '/php/tmp/session')) {
                session_save_path(ABS . '/php/tmp/session');

                //session_id('uiweb');
                session_name('uiweb');

                session_start();
            }else{
                die('Сессия не пишет');
            }
        }
    }

    //функция проверки авторизованности
    public function isAuth()
    {
        //проверяем сессию
        if(isset($this->session['auth']) && $this->session['auth'] == 1){
            $this->loginByUsername($this->session['username']);
            return 1;
        //проверяем куку
        }else if(isset($this->cookie['auth'])){
            //режем куку
            $data = explode(':', $this->cookie['auth']);

            //получаем логин
            $username = $data[0];
            //получаем hash
            $cookie_hash = $data[1];

            //получаем пользователя
            $user = (new User())->findByUsername($username);

            //получем hash пользователя
            $password_hash = $user->password;
            $cookie_hash_user = sha1($user->auth_key . ':' . $_SERVER['REMOTE_ADDR'] . ':' . $user->last_login_datetime . ':' . $password_hash);

            //проверям есть ли такой пользователь и совпадаeт ли hash
            if($user !== null && $cookie_hash == $cookie_hash_user){
                //логиним пользователя по куке
                $this->loginByUsername($user->username);
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function login($username, $password)
    {
        $password_hash = Hash::password($password);

        //текущая дата
        $current_time = time();

        //ищем пользователя по логину и паролю
        $user = (new User())->findUser($username, $password_hash);

        //проверка нашёлся ли пользователь
        if($user !== null){
            //$_SESSION['auth'] = 1;
            //$_SESSION['username'] = $user->username;

            $this->session['auth'] = 1;
            $this->session['username'] = $user->username;

            //устанавливаем куку
            $auth_key = uniqid();
            setcookie('auth', $user->username . ':' . sha1($auth_key . ':' . $_SERVER['REMOTE_ADDR'] . ':' . $current_time . ':' . $password_hash), time() + 60*60*60*24, '/');

            //записываем данные в пользователя для дальнейшей авторизации по куке
            $user->last_login_datetime = $current_time;
            $user->auth_key = $auth_key;
            $user->update();

			//записывае текущего пользователя
            $this->user = $user;
            return $user->id;
        }else{
            $this->session['auth'] = 0;
            return 0;
        }
    }

    public function loginByUsername($username)
    {
        //текущая дата
        $current_time = time();

        //ищем пользователя по логину
        $user = (new User())->findByUsername($username);

        //проверка нашёлся ли пользователь
        if($user !== null){
            $this->session['auth'] = 1;
            $this->session['username'] = $user->username;

            //записываем данные в пользователя для дальнейшей авторизации по куке
            $auth_key = uniqid();
            setcookie('auth', $user->username . ':' . sha1($auth_key . ':' . $_SERVER['REMOTE_ADDR'] . ':' . $current_time . ':' . $user->password), time() + 60*60*24, '/');

            //записываем данные в пользователя для дальнейшей авторизации по куке
            $user->last_login_datetime = $current_time;
            $user->auth_key = $auth_key;
            $user->update();

            $this->user = $user;
            return $user->id;
        }else{
            $this->session['auth'] = 0;
            return 0;
        }
    }

    public function logout()
    {
        setcookie('auth', null, 0, '/');
        session_destroy();
    }

    public function getUserField($field)
    {
        /*if(static::isAuth()){
            $result = (new User())->findByUsername($_SESSION['username'])->$field;
            return $result;
        }else{
            return false;
        }*/
    }
}