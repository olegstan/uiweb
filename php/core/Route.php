<?
namespace core;

use app\RouteAccess;
use app\system\ErrorController;

class Route
{
	static function start()
	{
		// контроллер и действие по умолчанию в ядре

		$core = Core::getCore()->init();

        //routing

		$i = 1;
		//обработка языка
		if($core->routes[$i] === 'en' || $core->routes[$i] === 'ru'){
			$core->language = $core->routes[$i];
			$i++;
		}

        //установка локали

        $current_language_key = array_search($core->language, $core->languages);
        $current_locale = $core->locales[$current_language_key];
        $set_locale_res = setlocale(LC_ALL, $current_locale . '.UTF8');

        if($set_locale_res == false){
            //echo 1;
            //echo '<!-- локаль не установлена -->';
        }else{

        }


        // получаем имя контроллера
		if(!empty($core->routes[$i]))
		{
			//роутинг для красивых урлов
			/*if($routes[1] == ''){

			}*/
			$core->controller_name = $core->routes[$i];
			$core->controller = $core->controller_name;
			$core->controller_name = ucfirst($core->controller_name);
			$i++;
		}

		// получаем имя экшена
		if(!empty($core->routes[$i]))
		{
			$core->action = $core->routes[$i];
			$core->action_uri = $core->routes[$i];
			//режем слова по '-'
			$action_words = explode('-', $core->action);

			//проверяем сколько слов
			if(count($action_words) > 1){
				$core->action = '';
				foreach($action_words as $k => $action_word){
					if($k == 0){
                        $core->action .= $action_word;
                    }else{
                        $core->action .= ucwords($action_word);
                    }
				}
			}
			$i++;
		}

		//проверяем есть ли четвертый параметр
		if(!empty($core->routes[$i]))
		{
			$core->request_id = $core->routes[$i];
		}

		// добавляем префиксы
		$core->controller_name = $core->controller_name . 'Controller';

		// подцепляем файл с классом контроллера
		$controller_file = $core->controller_name . '.php';
		$controller_path = ABS . '/php/app/controller/' . $controller_file;

        if(!isset($_SERVER['HTTP_DETECT_AJAX']) && !isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
            $start_time_script = microtime(true);
        }

        //проверяем роут
        (new RouteAccess())->check();

		if(file_exists($controller_path)){
			require_once($controller_path);
		}else{
            $error = '404';
            die((new ErrorController())->$error('Не существует контроллер'));
		}

		// создаем контроллер
        $controller = 'app\\controller\\' . $core->controller_name;
		$controller = new $controller;
		$action = $core->action;
		$core->layout = $core->controller;
		$core->default_layout = $core->controller;

		if(method_exists($controller, $action)){
			// вызываем действие контроллера
			if(isset($core->request_id)){
				echo $controller->$action($core->request_id);
			}else{
				echo $controller->$action();
			}

            if(!isset($_SERVER['HTTP_DETECT_AJAX']) && !isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
                $end_time_script = microtime(true);
                $diff = number_format(($start_time_script - $end_time_script), 3);
                echo '<!--' . $diff . ' sec -->';
                echo '<!--' . memory_get_usage(true) . ' bytes -->';
            }
		}else{
            $error = '404';
            die((new ErrorController())->$error('В контроллере не существует данного метода'));
		}
	}
}