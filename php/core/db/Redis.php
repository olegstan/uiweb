<?
namespace core\db;

class SocketException extends \Exception
{
}

class ProtocolException extends \Exception
{
}

class Redis
{

    const redis_status = '+';
    const redis_error = '-';
    const redis_integer = ':';
    const redis_bulk_string = '$';
    const redis_array = '*';

    const separator = "\r\n";

    private $socket;
    private $queue = [];
    private $pipelined = false;

    public function __construct($host = REDIS_HOST, $port = REDIS_POST, $timeout = null)
    {
        $timeout = $timeout ?: ini_get("default_socket_timeout");
        //$xportlist = stream_get_transports();

        //$this->socket = fsockopen($host, $port, $errno, $errstr, $timeout);
        //if (!$this->socket){
        //	throw new SocketException($errstr, $errno);
        //}
    }

    public function __destruct()
    {
        //fclose($this->socket);
    }

    public function get($k)
    {
        //формируем команду
        $cmd = '*2' . self::separator;
        $cmd .= '$3' . self::separator . 'GET' . self::separator;
        $cmd .= '$' . strlen($k) . self::separator . $k . self::separator;

        fwrite($this->socket, $cmd);

        return $this->response();
    }

    public function set($k, $v)
    {
        $cmd = '*3' . self::separator;
        $cmd .= '$3' . self::separator . 'SET' . self::separator;
        $cmd .= '$' . strlen($k) . self::separator . $k . self::separator;
        $cmd .= '$' . strlen($v) . self::separator . $v . self::separator;

        fwrite($this->socket, $cmd);

        return $this->response();
    }

    public function del($k)
    {
        $cmd = '*2' . self::separator;
        $cmd .= '$3' . self::separator . 'DEL' . self::separator;
        $cmd .= '$' . strlen($k) . self::separator . $k . self::separator;

        fwrite($this->socket, $cmd);

        return $this->response();
    }

    /**
     * @param $k
     * @return array|bool|int|null|string
     * @throws Exception
     * @throws RedisException
     *
     * количество букв 6
     */
    public function exists($k)
    {
        $cmd = '*2' . self::separator;
        $cmd .= '$6' . self::separator . 'EXISTS' . self::separator;
        $cmd .= '$' . strlen($k) . self::separator . $k . self::separator;

        fwrite($this->socket, $cmd);

        return $this->response();
    }

    private function response()
    {
        $reply = trim(fgets($this->socket, 512));

        // получаем первый знак
        switch (substr($reply, 0, 1)) {
            // error
            case '-':
                throw new RedisException(trim(substr($reply, 4)));
                break;
            // success
            case '+':
                $response = substr(trim($reply), 1);
                if ($response === 'OK') {
                    $response = true;
                }
                break;
            // Bulk reply
            case '$':
                $response = null;
                if ($reply == '$-1') {
                    break;
                }
                $read = 0;
                $size = intval(substr($reply, 1));
                if ($size > 0) {
                    do {
                        $block_size = ($size - $read) > 1024 ? 1024 : ($size - $read);
                        $r = fread($this->socket, $block_size);
                        if ($r === false) {
                            throw new Exception('Failed to read response from stream');
                        } else {
                            $read += strlen($r);
                            $response .= $r;
                        }
                    } while ($read < $size);
                }
                fread($this->socket, 2); // discard crlf
                break;
            // Multi-bulk reply
            case '*':
                $count = intval(substr($reply, 1));
                if ($count == '-1') {
                    return NULL;
                }
                $response = array();
                for ($i = 0; $i < $count; $i++) {
                    $response[] = $this->response();
                }
                break;
            // Integer reply
            case ':':
                $response = intval(substr(trim($reply), 1));
                break;
            default:
                throw new RedisException("Unknown response: {$reply}");
                break;
        }
        // Party on
        return $response;
    }

}


