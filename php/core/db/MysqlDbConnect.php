<?
namespace core\db;

use PDO;

class MysqlDbConnect
{
    private static $pdo = [];
    private static $connections = [];
    private $debug;

    public function __construct()
    {

        //$this->debug = new Debuger();
    }

    //запрещаем копирование объекта
    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    //по умолчанию 0 соединение
    public static function getPDO($connection)
    {
        $path = ABS . '/php/config/db.php';
        if(file_exists($path)){
            self::$connections = require_once($path);
        }else{
            die(__FILE__ . ' ' . __LINE__ . 'Не найден файл с соединениями: ' . $path);
        }

        if (!isset(self::$pdo[$connection])) {
            try {
                self::$pdo[$connection] = new PDO(self::$connections[$connection]['type'] . ":host=" . self::$connections[$connection]['host'] . ";dbname=" . self::$connections[$connection]['database'] . ";charset=" . self::$connections[$connection]['charset'], self::$connections[$connection]['username'], self::$connections[$connection]['password']);
            } catch (PDOException $e) {
                die('Connection error: ' . $e->getMessage());
            }
        }
        return self::$pdo[$connection];
    }

    public function getDebug()
    {
        return $this->debug;
    }

    public static function getConnections()
    {

    }
}
