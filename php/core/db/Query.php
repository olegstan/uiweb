<?
namespace core\db;

//use core\db\MysqlDbConnect;
use app\system\ErrorController;
use core\Collection;
use core\Core;
use PDO;

class Query extends MysqlDbConnect
{
    /**
     * @param $s_table_name
     * @param $s_class_name
     * @param null $a_join
     */

    public function __construct($i_connection, $s_table_name, $s_class_name, $a_join = null)
    {
        $this->core = Core::getCore();
        $this->s_table_name = $s_table_name;
        $this->s_class_name = $s_class_name;
        $this->a_join = $a_join;

        $this->i_connection = $i_connection;
    }

    /**
     * @param $field_names
     * @param $field_values
     *
     * //работа с БД
     *
     * //три типа вывода массива
     * //fetch
     * //PDO::FETCH_NUM
     * //PDO::FETCH_ASSOC
     * //PDO::FETCH_BOTH
     *
     * //fetchAll
     * //PDO::FETCH_COLUMN
     * //PDO::FETCH_CLASS
     * //PDO::FETCH_GROUP
     * //PDO::FETCH_UNIQUE
     * //PDO::FETCH_PROPS_LATE
     *
     * //fetchColumn
     *
     * //select по ID по массиву ID или по значению
     *
     **/
    private $s_table_name;
    private $s_class_name;

    private $a_parts = [];
    private $a_bind = [];
    //PDO statement
    private $stmt;

    public $fields = [];

    public $result;
    public $result_ids;
    public $result_fields;

    public function distinct()
    {
        $this->s_sql_distinct = 'DISTINCT ';
        return $this;
    }

    public function select($a_fields = null)
    {
        $this->a_parts[] = 'SELECT ';

        if(!empty($a_fields)){
            $i_count = count($a_fields) - 1;

            foreach ($a_fields as $i_k => $s_field) {
                $s_comma = ($i_count != $i_k) ? ', ' : ' ';
                $this->a_parts[] = '`' . $this->s_table_name . '`.`' . $s_field . '`' . $s_comma . ' ';
            }
        }else{
            $this->a_parts[] = '`' . $this->s_table_name . '`.* ';
        }



        if (!empty($this->a_join)) {
            $this->a_parts[] .= ', ';

            $i_count_iteration = 0;
            foreach($this->a_join as $a_table){
                $i_count_iteration += count($a_table['fields']);
            }

            $i_count_iteration_fields = 0;
            foreach ($this->a_join as $a_table) {
                foreach ($a_table['fields'] as $field => $alias) {
                    $i_count_iteration_fields++;
                    $s_comma = ($i_count_iteration != $i_count_iteration_fields) ? ', ' : ' ';
                    $this->a_parts[] = ' `' . $a_table['table_name'] . '`.`' . $field . '` AS ' . $alias . ' ' . $s_comma;
                }
            }

            $this->a_parts[] = 'FROM `' . $this->s_table_name . '` ';

            foreach ($this->a_join as $a_table) {
                $this->a_parts[] = '
					LEFT JOIN `' . $a_table['table_name'] . '`
					ON `' . $this->s_table_name . '`.`' . $a_table['key'] . '` = `' . $a_table['table_name'] . '`.`' . $a_table['foreign_key'] . '`
				';
            }
        }else{
            $this->a_parts[] = 'FROM `' . $this->s_table_name . '` ';
        }

        return $this;
    }

    /**
     * @param null $conditions
     * @param null $fields
     * @return $this
     *
     */

    public function where($conditions = null, $fields = null)
    {
        $this->a_parts[] = 'WHERE ';

        $this->a_parts[] .= $conditions;

        if ($fields !== null) {
            foreach ($fields as $k => $v) {
                $this->a_bind += [$k => $v];
            }
        }
        return $this;
    }

    /**
     * @param $field_names
     * @param $field_values
     */

    public function delete()
    {
        $this->a_parts[] = 'DELETE FROM `' . $this->s_table_name . '` ';
        return $this;
    }

    /**
     * @param $values - object
     */

    public function insert($object)
    {
        $values = get_object_vars($object);
        $column_names = $this->getColumnNames($this->s_table_name);

        //проверка есть ли такое поле в таблице
        foreach ($values as $k => $v) {
            if (in_array($k, $column_names) && $k !== 'id') {
                $this->fields[$k] = $v;
            }
        }

        $this->a_parts[] = 'INSERT INTO `' . $this->s_table_name . '` ';


        if (is_array($this->fields)) {
            $count = count($this->fields);

            end($this->a_parts);
            $last_key = key($this->a_parts);

            if ($count == 1) {
                $i = 1;
                foreach ($this->fields as $k => $v) {
                    $this->a_parts[$last_key + $i] = '(' . $k . ') ';
                    $this->a_parts[$last_key + $i + $count] = '(:' . $k . ');';
                    $this->a_bind += [':' . $k => $v];
                }
            } else {
                $count_for_values = 1;
                $i = 1;
                foreach ($this->fields as $k => $v) {
                    if ($i == 1) {
                        $this->a_parts[$last_key + $i] = '(' . $k . ', ';
                        $this->a_parts[$last_key + $i + $count + $count_for_values] = '(:' . $k . ', ';
                    } else if ($i == $count) {
                        $this->a_parts[$last_key + $i] = $k . ') ';
                        $this->a_parts[$last_key + $i + $count + $count_for_values] = ':' . $k . ');';
                    } else {
                        $this->a_parts[$last_key + $i] = $k . ', ';
                        $this->a_parts[$last_key + $i + $count + $count_for_values] = ':' . $k . ', ';
                    }
                    $this->a_bind += [':' . $k => $v];
                    $i++;
                }
            }
        }

        $this->a_parts[$last_key + $count + $count_for_values] = 'VALUES ';

        return $this;
    }

    /**
     * @param $field_names
     * @param $field_values
     */

    public function update($object)
    {
        if (is_object($object)) {
            $values = get_object_vars($object);
            $column_names = $this->getColumnNames($this->s_table_name);

            //проверка есть ли такое поле в таблице
            foreach ($values as $k => $v) {
                if (in_array($k, $column_names) && $k !== 'id') {
                    $this->fields[$k] = $v;
                }
            }
        }

        $this->a_parts[] = 'UPDATE `' . $this->s_table_name . '` ';
        $this->a_parts[] = 'SET ';

        if (is_array($this->fields)) {

            $count = count($this->fields);

            if ($count == 1) {
                foreach ($this->fields as $k => $v) {
                    $this->a_parts[] .= $k . ' = :' . $k . ' ';
                    $this->a_bind += [':' . $k => $v];
                }
            } else {
                $i = 1;
                foreach ($this->fields as $k => $v) {
                    if ($i == 1) {
                        $this->a_parts[] .= $k . ' = :' . $k . ', ';
                    } else if ($i == $count) {
                        $this->a_parts[] .= $k . ' = :' . $k . ' ';
                    } else {
                        $this->a_parts[] .= $k . ' = :' . $k . ', ';
                    }
                    $this->a_bind += [':' . $k => $v];
                    $i++;
                }
            }
        }

        return $this;
    }

    public function sql($s_sql)
    {
        $this->a_parts[] = $s_sql;
        return $this;
    }


    public function getTables()
    {
        $stmt = self::getPDO($this->i_connection)->prepare('SHOW TABLES');
        $stmt->execute();

        foreach ($stmt->fetchAll() as $table) {
            $table_names[] = $table[0];
        }
        return $table_names;
    }

    public function getColumnNames()
    {
        //проверяем не пустая ли имя таблицы
        if($this->s_table_name == null){
            die('нет названия таблицы' . $this->s_table_name);
        }

        /**
         * написать проерку если вернулась пустой массив
         */

        $stmt = self::getPDO($this->i_connection)->prepare('SHOW COLUMNS FROM `' . $this->s_table_name . '`');
        $stmt->execute();

        foreach ($stmt->fetchAll() as $field) {
            $field_names[] = $field[0];
        }
        return $field_names;
    }

    public function execute()
    {
        $query = '';
        ksort($this->a_parts);
        foreach($this->a_parts as $part){
            $query .= $part;
        }

        $this->stmt = self::getPDO($this->i_connection)->prepare($query);

        //echo '<!--' . $query . '<br> -->';

        if($this->core->auth->is_auth && 1){
            /*echo '<pre>';
            var_dump(!empty($this->a_parts));
            var_dump($this->a_parts);
            var_dump($this->s_sql);
            echo '</pre>';*/

            //echo '<!--' . $this->s_sql . '<br> -->';
        }

        //echo $this->s_sql . '<br>';
        //ловим ошибки
        if (!$this->stmt->execute($this->a_bind)) {
            $error = $this->stmt->errorInfo();

            if (isset($error[1])) {
                // 1054 - Unknown column
                // 1064 - Syntax error
                // 1062 - Duplicate entry
                // 1048 - mot Null
                // 1046 - table does not exist
                // 1265 - Data truncated
                //if ($error[1] == 1062 || $error[1] ==  1048 || $error[1] ==  1265){

                $error_action = '500';
                die((new ErrorController())->$error_action('Ошибка запроса к БД ' . $error[2] . '<br>' . $query));
            }
        }

        //clear all
        $this->a_parts = [];
        $this->a_bind = [];
        return $this;
    }

    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @return array|string
     */
    public function one($rules = null)
    {
        $objects = $this->stmt->fetchAll(PDO::FETCH_CLASS, $this->s_class_name);
        $collection = new Collection();

        foreach ($objects as $key => $object) {
            $collection->appendOne($object->beforeSelect($rules), $object->id);
            break;
        }

        $this->stmt->closeCursor();

        return $collection;
    }

    /**
     * @return $this
     * result not by id them set ture
     */

    public function all($rules = null, $key = null)
    {
        $objects = $this->stmt->fetchAll(PDO::FETCH_CLASS, $this->s_class_name);
        $collection = new Collection();

        if ($key) {
            foreach ($objects as $key => $object) {
                $collection->appendToArray($object->beforeSelect($rules));
            }
        } else {
            foreach ($objects as $key => $object) {
                $collection->appendToArray($object->beforeSelect($rules), $object->id);
            }
        }

        $this->stmt->closeCursor();

        return $collection;
    }

    public function order($field_name, $sort = 'ASC')
    {
        $this->s_sql_order = ' ORDER BY `' . $this->s_table_name .'`.`' . $field_name . '` ' . $sort;
        return $this;
    }

    public function limit($start = 0, $offest = 1)
    {
        $this->s_sql_limit = ' LIMIT ' . $start . ', ' . $offest;
        return $this;
    }

    public function getlastInsertId()
    {
        $last_inserted_id = self::getPDO($this->i_connection)->lastInsertId();
        return $last_inserted_id;
    }

    //rowCount() количество задетых строк

    public function dump()
    {
        $dump_dir = ABS . '/tmp/schema/'; // директория, куда будем сохранять резервную копию БД

        foreach($this->getTables() as $table){
            $fp = fopen( $dump_dir."/".$table[0].".sql", "a" );



        }


        /*while( $table = mysql_fetch_row($res) )
        {
            $fp = fopen( $dump_dir."/".$table[0].".sql", "a" );
            if ( $fp )
            {
                $query = "TRUNCATE TABLE `".$table[0]."`;\n";
                fwrite ($fp, $query);
                $rows = 'SELECT * FROM `'.$table[0].'`';
                $r = mysql_query($rows) or die("Ошибка при выполнении запроса: ".mysql_error());
                while( $row = mysql_fetch_row($r) )
                {
                    $query = "";
                    foreach ( $row as $field )
                    {
                        if ( is_null($field) )
                            $field = "NULL";
                        else
                            $field = "'".mysql_escape_string( $field )."'";
                        if ( $query == "" )
                            $query = $field;
                        else
                            $query = $query.', '.$field;
                    }
                    $query = "INSERT INTO `".$table[0]."` VALUES (".$query.");\n";
                    fwrite ($fp, $query);
                }
                fclose ($fp);
            }
        }*/
    }

}