<?
namespace core;

use core\db\Query;
use core\db\Redis;
use core\helper\Inflector;
use core\helper\Validation;

abstract class Model
{
    /**
     * @var
     */

    protected $core;

    public $connection = 0;
    public $inserted_fields = [];
    public $updated_fields = [];

    /*static $instance;*/

    public static function self()
    {
        //return new get_called_class();
    }

    //protected $query;
    /**
     *
     */
    
    /*public function get($property)
    {
        return $this->$property;
    }*/

    public function __construct($rules = null)
    {
        $this->core = Core::getCore();
        $this->query = new Query($this->connection, $this->table, get_class($this), $this->join($rules));
        $this->redis = new Redis();
        $this->validation = new Validation();
    }

    public function getTable()
    {
        return Inflector::tableize(get_class($this));
    }

    /**
     * @param $id
     * @return array|string
     */

    public function findById($id)
    {
        if (is_array($id)) {
            return $this->query->select()->where('`' . $this->table . '`.`id` IN (' . implode(',', $id) . ')')->execute();
        } else {
            return $this->query->select()->where('`' . $this->table . '`.`id` = :id', [':id' => $id])->limit()->execute();
        }
    }

    public function findByField($field, $value)
    {
        return $this->query->select()->where('`' . $this->table . '`.`' . $field . '` = :' . $field, [':' . $field => $value])->execute();
    }

    /**
     * @return array
     */

    public function findAll($rules = null, $key = false)
    {
        return $this->query->select()->execute()->all($rules, $key);
    }

    /**
     * @return $this
     */

    public function findOwn()
    {
        return $this->query->select()->where('user_id = :user_id', [':user_id' => $this->core->auth->user->id])->execute();
    }

    /**
     *
     */

    public function select()
    {

    }

    /**
     * @param bool $id
     * @return $this
     */

    public function insert()
    {
        $this->validate();

        $this->beforeInsert();

        $this->fields = $this->query->insert($this)->execute()->getFields();

        $this->fields['id'] = $this->id = $this->query->getlastInsertId();

        $this->afterInsert();

        return ['result' => 'success', 'fields' => $this->fields];
    }

    /**
     * @return array
     */

    public function update()
    {
        $this->validate();

        $this->fields = $this->query->update($this)->where('id = :id', [':id' => $this->id])->execute()->getFields();

        $this->fields['id'] = $this->id;

        return ['result' => 'success', 'fields' => $this->fields];
    }

    public function updateOwn()
    {
        if ($this->core->auth->is_auth) {
            $this->validate();

            $this->query->update($this)->where('id = :id AND user_id = :user_id', [':id' => $this->id, ':user_id' => $this->core->auth->user->id])->execute();

            return ['result' => 'success', 'fields' => $this];
        } else {
            return ['result' => 'error', 'errors' => ['auth' => 'Пользователь не авторизован']];
        }
    }

    /**
     * @param $id
     */

    public function delete()
    {
        $this->query->delete()->where('id = :id', [':id' => $this->id])->execute();

        $this->fields['id'] = $this->id;

        $this->afterDelete();

        return ['result' => 'success', 'fields' => $this->fields];
    }

    /**
     * @param $id
     */

    public function deleteOwn()
    {
        if ($this->core->auth->is_auth) {
            $this->query->delete()->where('id = :id AND user_id = :user_id', [':id' => $this->id, ':user_id' => $this->core->auth->user->id])->execute();
            $this->afterDelete();
            return ['result' => 'success', 'fields' => $this];
        } else {
            return ['result' => 'error', 'errors' => ['auth' => 'Пользователь не авторизован']];
        }
    }

    /**
     * @param null $rules
     * @return $this
     *
     * условие обработки селекта
     */

    public function beforeSelect($rules = null)
    {
        return $this;
    }

    /**
     *
     */

    public function beforeInsert()
    {
        return $this;
    }

    /**
     *
     */

    public function afterInsert()
    {
        return $this;
    }

    /**
     * @param null $scenario
     * @return array
     */

    public function validate($scenario = null)
    {
        $validate_rules = $this->validateRules($scenario);

        if ($validate_rules) {
            foreach ($validate_rules as $rules) {
                if (isset($rules['field'])) {
                    if (is_array($rules['rule'])) {
                        foreach ($rules['rule'] as $rule) {

                        }
                    } else if (is_string($rules['rule'])) {
                        //echo $rules['rule'] . '--' . $rules['field'] .  '<br>';
                        switch ($rules['rule']) {
                            case 'empty':
                                $this->validation->validateEmpty($this->$rules['property'], $rules['field'], $rules['msg']);
                                break;
                            case 'integer':
                                $this->validation->validateInteger($this->$rules['property'], $rules['field'], $rules['msg']);
                                break;
                            case 'string':
                                $this->validation->validateString($this->$rules['property'], $rules['field'], $rules['msg']);
                                break;
                            case 'max_length':
                                $this->validation->validateMaxLength($this->$rules['property'], $rules['field'], $rules['length'], $rules['msg']);
                                break;
                            case 'min_length':
                                $this->validation->validateMinLength($this->$rules['property'], $rules['field'], $rules['length'], $rules['msg']);
                                break;
                            case 'max_value':
                                $this->validation->validateMaxValue($this->$rules['property'], $rules['field'], $rules['max_value'], $rules['msg']);
                                break;
                            case 'min_value':
                                $this->validation->validateMinValue($this->$rules['property'], $rules['field'], $rules['min_value'], $rules['msg']);
                                break;
                            case 'email':
                                $this->validation->validateEmailPattern($this->$rules['property'], $rules['field'], $rules['msg']);
                                break;
                            case 'unique':
                                $this->validation->validateUniqueField($this->$rules['property'], $rules['field'], $rules['class_name'], $rules['class_field'], $rules['table_field'], $rules['msg']);
                                break;
                            case 'unique_fields':
                                foreach ($rules['property'] as $property) {
                                    $validate_property[] = $this->$property;
                                }
                                $this->validation->validateUniqueFields($validate_property, $rules['field'], $rules['class_name'], $rules['class_fields'], $rules['table_fields'], $rules['msg']);
                                break;
                            case 'pattern':
                                $this->validation->validatePattern($this->$rules['property'], $rules['field'], $rules['pattern'], $rules['msg']);
                                break;
                            case 'dynamic_pattern':
                                $this->validation->validateDynamicPattern($this->$rules['property'], $rules['field'], $rules['class_name'], $rules['class_id'], $rules['msg']);
                                break;
                            case 'compare':
                                $this->validation->validateCompare($this->$rules['property'], $this->$rules['another_property'], $rules['field'], $rules['another_field'], $rules['msg']);
                                break;
                        }
                    }
                }
            }
        }

        $this->validation->a_success = array_diff_key($this->validation->a_success, $this->validation->a_errors);

        return $this->validation->a_errors;
    }

    /**
     *
     */

    public function beforeValidate()
    {

    }

    /**
     *
     */

    public function afterValidate()
    {

    }

    /**
     *
     */

    public function afterDelete()
    {

    }

    /**
     *
     */

    public function beforeDelete()
    {

    }

    /**
     * @param $array
     * @param null $scenario
     * @return $this
     */

    public function load($array, $scenario = null)
    {
        $rules = $this->rules($scenario);

        foreach ($rules as $rule) {
            $this->$rule['field'] = $this->prepare($rule, $array[$rule['field']]);
        }
        return $this;
    }

    public function selectRules($scenario)
    {
        return [];
    }

    public function rules($scenario)
    {
        return [];
    }

    public function validateRules($scenario)
    {
        return [];
    }

    public function join($rules = null)
    {

    }

    /**
     * @param $array
     * @param $value
     * @return mixed
     */

    public function prepare($array, $value)
    {
        if (isset($array['field'])) {
            if (is_array($array['filter'])) {
                foreach ($array['filter'] as $filter) {
                    $value = $array['filter'][$filter]($value);
                }
            } else if (is_string($array['filter'])) {
                $value = $array['filter']($value);
            }
        }
        return $value;
    }

    /**
     * @param $array
     * @return string
     */

    public function ajaxMethod($array)
    {
        $this->load($array, $array['action']);

        switch ($array['action']) {
            case 'insert':
                $result = $this->insert();
                break;
            case 'update':
                $result = $this->update();
                break;
            case 'delete':
                $result = $this->delete();
                break;
        }


        echo json_encode($result);
    }
}
