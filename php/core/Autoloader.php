<?
class Autoloader {

    public static $loader;

    public static function init()
    {
        if(self::$loader == null){
            self::$loader = new self();
        }

        return self::$loader;
    }

    public function __construct()
    {
        spl_autoload_register([$this, 'core']);
    }

    public function core($class)
    {
        $parts = explode('\\', $class);
        $count_parts = count($parts);

        $file_path = ABS . '/php/';

        $i = 0;
        foreach($parts as $part){
            if($i !== ($count_parts - 1)){
                $file_path .= $parts[$i] . '/';
            }else{
                $file_path .= $parts[$i] . '.php';
            }
            $i++;
        }

        if(file_exists($file_path)){
            require_once($file_path);
        }

    }

}