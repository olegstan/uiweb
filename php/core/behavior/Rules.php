<?
namespace core\behavior;

interface Rules
{
    /**
     * @param $scenario
     * @return mixed
     */

    function rules($scenario);

    /**
     * @param $scenario
     * @return mixed
     */

    function validateRules($scenario);
}