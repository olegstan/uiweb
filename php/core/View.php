<?
namespace core;

use core\helper\Render;
use core\helper\Url;

class View
{
    public $content;
    public $description;
    public $keywords;
    public $title;

    public function __construct()
    {
        $this->core = Core::getCore();
        $this->layout = new Render('layout');
        $this->view = new Render('view');
        $this->component = new Render('component');
        $this->url = new Url();
    }

    public function getDescription()
    {
        return '<meta name="description" content="' . $this->core->description . '">';
    }

    public function getKeywords()
    {
        return '<meta name="keywords" content="' . $this->core->keywords . '">';
    }

    public function getTitle()
    {
        return '<title>' . $this->core->title . '</title>';
    }

    public function getAlternate()
    {
        $languages = $this->core->languages;
        if (($key = array_search($this->core->language, $languages)) !== false) {
            unset($languages[$key]);
        }

        $result = '';
        foreach ($languages as $language) {
            $result .= '<link rel="alternate" hreflang="' . $language . '" href="http://uiweb.ru/en" />' . "\n";
        }
        return $result;
    }

    public function renderView($data = null)
    {
        $this->view->render($this->core->action_uri, $data);
    }
}