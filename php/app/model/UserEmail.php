<?
namespace app\model;

use app\layer\LayerModel;

class UserEmail extends LayerModel
{
    public $table = 'user-emails';

    public $domain;

    public function join()
    {
        return [
            [
                'class_name' => 'Domain',
                'table_name' => 'domains',
                'key' => 'domain_id',
                'foreign_key' => 'id',
                'fields' => [
                    'domain' => 'domain'
                ]
            ]
        ];
    }
}