<?
namespace app\model;

use app\layer\LayerModel;
use core\behavior\Rules;

class Country extends LayerModel implements Rules
{
    public $table = 'countries';

    /**
     * @param $scenario
     * @return array|mixed
     */

    public function rules($scenario)
    {
        switch ($scenario) {
            case 'insert':
                return [
                    ['field' => 'name', 'filter' => 'trim']
                ];
                break;
            case 'update':
                return [
                    ['field' => 'id', 'filter' => 'trim'],
                    ['field' => 'name', 'filter' => 'trim'],
                ];
                break;
            case 'delete':
                return [
                    ['field' => 'id', 'filter' => 'trim'],
                ];
                break;
        }
    }

    /**
     * @param $scenario
     * @return mixed|void
     */

    public function validateRules($scenario)
    {

    }

    public function join()
    {

    }
}