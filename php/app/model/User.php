<?
namespace app\model;

use app\layer\LayerModel;
use core\behavior\Rules;
use core\helper\Hash;
use core\Response;

class User extends LayerModel implements Rules
{
    public $table = 'users';

    public $login;
    public $domain;

    public $password;
    public $auth_key;
    public $permission;
    public $last_login_datetime;

    /**
     * @param string $scenario
     * @return array
     */

    public function rules($scenario)
    {
        switch ($scenario) {
            case 'register':
                return [
                    ['field' => 'username', 'filter' => 'trim'],
                    ['field' => 'email', 'filter' => 'trim'],
                    ['field' => 'password', 'filter' => 'trim'],
                    ['field' => 'password_repeat', 'filter' => 'trim'],
                ];
            case 'login':
                return [
                    ['field' => 'username', 'filter' => 'trim'],
                    ['field' => 'password', 'filter' => 'trim'],
                ];
            case 'change-password':
                return [
                    ['field' => 'old_password', 'filter' => 'trim'],
                    ['field' => 'new_password', 'filter' => 'trim'],
                    ['field' => 'new_password_repeat', 'filter' => 'trim'],
                ];
            case 'insert':
                return [
                    ['field' => 'username', 'filter' => 'trim'],
                    ['field' => 'email', 'filter' => 'trim'],
                    ['field' => 'password', 'filter' => 'trim'],
                    ['field' => 'auth_key', 'filter' => 'trim'],
                    ['field' => 'permission', 'filter' => 'trim'],
                    ['field' => 'last_login_datetime', 'filter' => 'trim'],
                    ['field' => 'created_at', 'filter' => 'trim'],
                    ['field' => 'modified_at', 'filter' => 'trim'],
                ];
            case 'update':
                return [
                    ['field' => 'id', 'filter' => 'trim'],
                    ['field' => 'username', 'filter' => 'trim'],
                    ['field' => 'email', 'filter' => 'trim'],
                    ['field' => 'password', 'filter' => 'trim'],
                    ['field' => 'auth_key', 'filter' => 'trim'],
                    ['field' => 'permission', 'filter' => 'trim'],
                    ['field' => 'last_login_datetime', 'filter' => 'trim'],
                    ['field' => 'created_at', 'filter' => 'trim'],
                    ['field' => 'modified_at', 'filter' => 'trim'],
                ];
            case 'delete':
                return [
                    ['field' => 'id', 'filter' => 'trim'],
                ];
        }
    }

    public function validateRules($scenario)
    {
        switch ($scenario) {
            case 'login':
                return [
                    ['field' => 'username', 'property' => 'username', 'rule' => 'empty', 'msg' => 'Введите логин'],
                    ['field' => 'password', 'property' => 'password', 'rule' => 'empty', 'msg' => 'Введите пароль'],
                ];
            case 'register':
                return [
                    ['field' => 'username', 'property' => 'username', 'rule' => 'unique', 'class_name' => 'User', 'class_field' => 'username', 'table_field' => '`users`.`username`', 'msg' => 'Этот логин уже используется'],
                    ['field' => 'username', 'property' => 'username', 'rule' => 'max_length', 'length' => 50, 'msg' => 'Логин не должен содержать больше 50 символов'],
                    ['field' => 'username', 'property' => 'username', 'rule' => 'empty', 'msg' => 'Напишите логин'],
                    ['field' => 'email', 'property' => ['login', 'domain'], 'rule' => 'unique_fields', 'class_name' => 'UserEmail', 'class_fields' => ['login', 'domain'], 'table_fields' => ['`user-emails`.`login`', '`domains`.`domain`'], 'msg' => 'Этот адрес электронной почты уже используется'],
                    ['field' => 'email', 'property' => 'email', 'rule' => 'email', 'msg' => 'Адрес электронной почты не соответствует формату'],
                    ['field' => 'email', 'property' => 'email', 'rule' => 'max_length', 'length' => 253, 'msg' => 'Адрес электронной почты не может содержать больше 320 символов'],
                    ['field' => 'email', 'property' => 'email', 'rule' => 'empty', 'msg' => 'Напишите адрес электронной почты'],
                    ['field' => 'password', 'another_field' => 'password_repeat', 'property' => 'password', 'another_property' => 'password_repeat', 'rule' => 'compare', 'msg' => 'Пароли не совпадают'],
                    ['field' => 'password', 'property' => 'password', 'rule' => 'empty', 'msg' => 'Придумайте пароль'],
                    ['field' => 'password_repeat', 'property' => 'password_repeat', 'rule' => 'empty', 'msg' => 'Подтверждение пароля не может быть пустым'],
                ];
            case 'change-password':
                return [
                    ['field' => 'old_password', 'property' => 'old_password', 'rule' => 'empty', 'msg' => 'Введите старый пароль'],
                    ['field' => 'new_password', 'property' => 'new_password', 'rule' => 'empty', 'msg' => 'Введите новый пароль'],
                    ['field' => 'new_password_repeat', 'property' => 'new_password_repeat', 'rule' => 'empty', 'msg' => 'Повторно введите новый пароль'],
                    ['field' => 'new_password', 'another_field' => 'new_password_repeat', 'property' => 'new_password', 'another_property' => 'new_password_repeat', 'rule' => 'compare', 'msg' => 'Пароли не совпадают'],
                ];
        }
    }

    public function join()
    {

    }

    public function findByUsername($username)
    {
        return $this->query->select()->where('`users`.`username` = :username', [':username' => $username])->execute()->one()->getResult();
    }

    public function findUser($username, $password)
    {
        //написать защищённый запрос
        return $this->query->select()->where('`users`.`username` = :username AND `users`.`password` = :password', [':username' => $username, ':password' => $password])->limit()->execute()->one()->getResult();
    }

    public function register()
    {
        $email_parts = explode('@', $this->email);
        //email
        $this->login = $email_parts[0];
        $this->domain = $email_parts[1];
        if (count($this->validate('register'))) {
            Response::json(['result' => 'error', 'error' => 'Поля заполнены неверно', 'success' => $this->validation->a_success, 'errors' => $this->validation->a_errors]);
        } else {
            //email save
            $domian = (new Domain())->query->select()->where('`domains`.`domain` = :domain', [':domain' => $this->domain])->execute()->one()->getResult();

            if ($domian == null) {
                $domian = new Domain();
                $domian->domain = $this->domain;
                $domian->insert();
            }

            $user_email = (new UserEmail())->query->select()->where('`user-emails`.`login` = :login AND `domains`.`id` = :id', [':login' => $email_parts[0], ':id' => $domian->id])->execute()->one()->getResult();

            if ($user_email == null) {

                //user save
                $this->password = Hash::password($this->password);
                $this->permission = 0;
                $this->status = 0;
                $this->created_at = time();
                $this->insert();
                //user save

                //auth
                $this->core->auth->loginByUsername($this->username);

                //email save
                $user_email = new UserEmail();
                $user_email->user_id = $this->core->auth->user->id;
                $user_email->domain_id = $domian->id;
                $user_email->login = $this->login;
                $user_email->status = 0;
                $user_email->insert();
                //email save
            }

            /**
             * send email
             */
            $this->core->flash->set('auth', 'Вы успешно зарегистрировались');

            (new Mail())->send($this->email, 'Регистрация', 'register');

            Response::json(['result' => 'success', 'success' => 'Вы успешно зарегистрировались']);
        }
    }

    public function login()
    {
        $error_message = 'Неверная комбинация логина и пароля';
        if (count($this->validate('login'))) {
            Response::json(['result' => 'error', 'error' => $error_message, 'success' => $this->validation->a_success, 'errors' => $this->validation->a_errors]);
        } else {
            if ($this->core->auth->login($this->username, $this->password)) {
                $this->core->flash->set('auth', 'Вы успешно авторизовались');
                Response::json(['result' => 'success', 'success' => 'Вы успешно авторизовались']);
            } else {
                Response::json(['result' => 'error', 'error' => $error_message]);
            }
        }
    }

    public function changePassword()
    {
        $password = $this->old_password;
        $user = $this->core->auth->user;
        $current_password = $user->password;

        if(Hash::verify($password, $current_password)){
            if(count($this->validate('change-password'))){
                Response::json(['result' => 'error', 'error' => 'Поля заполнены неверно', 'success' => $this->validation->a_success, 'errors' => $this->validation->a_errors]);
            }else{
                $user->password = Hash::password($this->new_password);
                $user->insert();

                $this->core->flash->set('auth', 'Вы успешно изменили пароль');
                Response::json(['result' => 'success', 'success' => 'Вы успешно изменили пароль']);
            }
        }else{
            $message = 'Неправильный старый пароль';
            Response::json(['result' => 'error', 'error' => $message, 'errors' => ['old_password' => $message]]);
        }
    }

    public function beforeSave()
    {

    }


    /*
     * возможно будет передавать
     * параметр для определения размера картинки
     */
    public $default_avatar = '';

    public function getAvatar()
    {
        return isset($this->avatar) ? $this->avatar : $this->default_avatar;
    }

    /*public function getName()
    {
        return isset() ?
    }*/
}