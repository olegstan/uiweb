<?
namespace app\asset;

use app\layer\LayerAssetBundle;

class MainAssetBundle extends LayerAssetBundle
{
    public $headerCSS = [
        '/php/assets/common/css/font-awesome.css',
        '/php/assets/common/css/fonts.css',
        '/php/assets/common/css/normalize.css',
        '/php/assets/common/css/custom-bootstrap.css',
        '/php/assets/common/css/header.css',
        '/php/assets/common/css/footer.css',
        '/php/assets/common/css/flash.css',
        '/php/assets/main/css/main.css',
        '/php/assets/component/carousel/css/carousel.css',
        '/php/assets/component/form-feedback/css/form-feedback.css',
        '/php/assets/component/modal/css/modal.css',
        '/php/assets/component/preloader/css/preloader.css'
    ];

    public $footerJS = [
        '/php/assets/common/js/system.js',
        '/php/assets/common/js/core.js',
        '/php/assets/common/js/keymap.js',
        '/php/assets/component/carousel/js/carousel.js',
        '/php/assets/common/js/scroll.js',
        '/php/assets/common/js/ajaxform.js',
        '/php/assets/common/js/mask.js',
        '/php/assets/common/js/smartform.js',
        '/php/assets/common/js/clearinput.js',
        '/php/assets/common/js/dropdown.js',
        '/php/assets/common/js/customselect.js',
        '/php/assets/component/modal/js/modal.js',
        '/php/assets/component/preloader/js/preloader.js'
    ];
}