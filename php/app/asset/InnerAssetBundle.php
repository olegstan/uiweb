<?
namespace app\asset;

use app\layer\LayerAssetBundle;

class InnerAssetBundle extends LayerAssetBundle
{
    public $headerCSS = [
        '/php/assets/common/css/font-awesome.css',
        '/php/assets/common/css/fonts.css',
        '/php/assets/common/css/normalize.css',
        '/php/assets/common/css/custom-bootstrap.css',
        '/php/assets/common/css/header.css',
        '/php/assets/common/css/footer.css',
        '/php/assets/common/css/flash.css',
        '/php/assets/inner/css/inner.css',
        '/php/assets/component/modal/css/modal.css'
    ];

    public $footerJS = [
        '/php/assets/common/js/system.js',
        '/php/assets/common/js/core.js',
        '/php/assets/component/modal/js/modal.js',
        '/php/assets/component/preloader/js/preloader.js'
    ];
}