<form id="form-login" method="post" action="<?=$this->url->route('ajax', 'form-login')?>">
    <h1 class="text-center">Войти на сайт</h1>
    <h2 class="text-center">Введите логин и пароль для авторизации</h2>
    <div class="alert alert-success" id="success">
        <small></small>
    </div>
    <div class="alert alert-error" id="error">
        <small></small>
    </div>
    <div class="input-container">
        <input id="username" type="text" name="username" placeholder="Имя пользователя">
        <img data-clear-input="username" class="input-clear" src="/php/assets/component/modal/img/close.png">
    </div>
    <div class="alert alert-error" data-error="username">
        <small></small>
    </div>
    <div class="input-container">
        <input id="password" type="password" name="password" placeholder="Пароль">
        <img data-show-input="password" data-show-input-opened="/php/assets/common/img/closed_eye.png" data-show-input-closed="/php/assets/common/img/eye.png" class="show-password" src="/php/assets/common/img/eye.png">
    </div>
    <div class="alert alert-error" data-error="password">
        <small></small>
    </div>
    <!--<input type="checkbox" value="remember">-->
    <div class="row text-center">
        <input class="btn btn-order" type="submit" value="Войти">
    </div>
</form>