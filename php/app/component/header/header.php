<header id="header">
	<div id="menu">
		<div class="container-custom">
			<div id="logo"><a href="<?=$this->url->home()?>">UIweb</a></div>
			<ul>
				<li>
					<a class="header-link" href="<?=$this->url->home()?>"><?=$t['main']?></a>
				</li>
				<li>
					<a class="header-link" href="<?=$this->url->route('blog')?>"><?=$t['blog']?></a>
				</li>
				<li>
					<a class="header-link" href="<?=$this->url->route('main', 'services')?>"><?=$t['services']?></a>
				</li>
				<li>
					<a class="header-link" href="<?=$this->url->route('main', 'contacts')?>"><?=$t['contacts']?></a>
				</li>
				<li>
					<a href="<?=$this->url->home()?>#form-feedback-block" data-href="form-feedback-block" class="btn btn-order"><?=$t['order']?></a>
				</li>
			</ul>
		</div>
	</div>
</header>