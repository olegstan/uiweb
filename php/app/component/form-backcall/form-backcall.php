<div class="page">
    <form id="form-backcall" action="<?=$this->url->route('ajax', 'form-backcall')?>" method="post" autocomplete="off">
        <h1 class="text-center">Мы можем сами вам перезвонить!</h1>
        <h2 class="text-center">Мы перезвоним в ближайшее время</h2>
        <div class="alert alert-success" id="success">
            <small></small>
        </div>
        <div class="alert alert-error" id="error">
            <small></small>
        </div>
        <div class="input-container">
            <input type="text" name="name" id="name" placeholder="Как к вам можно обращаться">
            <img data-clear-input="name" class="input-clear" src="/php/assets/component/modal/img/close.png">
        </div>
        <div class="alert alert-error" data-error="name">
            <small></small>
        </div>
        <div class="input-container">
            <!-- Dropdown menu start -->

            <div id="phone-code" data-country-id="1" data-country-pattern="\(\d\d\d\)\d\d\d-\d\d-\d\d">
                <div class="country-flag" style="background-position: 0 -660px"></div>
                <span class="country-code">+7</span>
                <div class="clearfix"></div>
            </div>
            <div id="phone-code-container">
                <div id="phone-code-search-container">
                    <input autocomplete="off" type="text" name="q" id="country-search" placeholder="Поиск">
                </div>
                <div id="phone-countries-container">
                    <? foreach($countries as $country){ ?>
                        <div class="phone-container" data-country-id="<?=$country->id?>" data-country-name="<?=$country->name?>" data-country-alias="<?=$country->alias?>" data-country-pattern="<?=$country->pattern?>" data-country-code="<?=$country->code?>" data-country-flag="<?=$country->flag?>">
                            <div class="country-flag" style="background-position: 0 <?=$country->flag?>px"></div>
                            <span class="country-name"><?=$country->name?></span>
                            <span class="country-code"><?=$country->code?></span>
                            <div class="clearfix"></div>
                        </div>
                    <? } ?>
                </div>
            </div>

            <!-- Dropdown menu end -->

            <input type="text" name="phone" id="phone" placeholder="Телефон" autocomplete="off">
            <img data-clear-input="phone" class="input-clear" src="/php/assets/component/modal/img/close.png">
        </div>
        <div class="clearfix"></div>
        <div class="alert alert-error" data-error="phone">
            <small></small>
        </div>
        <div class="row text-center">
            <input class="btn btn-order" type="submit" value="Отправить">
        </div>
    </form>
</div>