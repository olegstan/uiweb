<div id="modal">
    <div id="modal-background"></div>
    <div id="popup">
        <div id="popup-content">
            <div class="popup-header">
                <div class="popup-close" onclick="core.modal.close(event);"></div>
                <div id="popup-title">Модальное окно</div>
            </div>
            <div class="popup-inner-container">
                <div id="popup-inner">

                </div>
            </div>
            <div class="popup-footer"></div>
        </div>
    </div>
</div>