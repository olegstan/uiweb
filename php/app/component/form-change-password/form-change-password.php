<form id="form-change-password" method="post" action="<?=$this->url->route('ajax', 'form-change-password')?>">
    <h1 class="text-center">Изменить пароль</h1>
    <div class="alert alert-success" id="success">
        <small></small>
    </div>
    <div class="alert alert-error" id="error">
        <small></small>
    </div>
    <div class="input-container">
        <input id="old_password" type="password" name="old_password" placeholder="Старый пароль">
        <img data-show-input="old_password" data-show-input-opened="/php/assets/common/img/closed_eye.png" data-show-input-closed="/php/assets/common/img/eye.png" class="show-password" src="/php/assets/common/img/eye.png">
    </div>
    <div class="alert alert-error" data-error="old_password">
        <small></small>
    </div>
    <div class="input-container">
        <input id="new_password" type="password" name="new_password" placeholder="Новый пароль">
        <img data-show-input="new_password" data-show-input-opened="/php/assets/common/img/closed_eye.png" data-show-input-closed="/php/assets/common/img/eye.png" class="show-password" src="/php/assets/common/img/eye.png">
    </div>
    <div class="alert alert-error" data-error="new_password">
        <small></small>
    </div>
    <div class="input-container">
        <input id="new_password_repeat" type="password" name="new_password_repeat" placeholder="Повтор нового пароля">
        <img data-show-input="new_password_repeat" data-show-input-opened="/php/assets/common/img/closed_eye.png" data-show-input-closed="/php/assets/common/img/eye.png" class="show-password" src="/php/assets/common/img/eye.png">
    </div>
    <div class="alert alert-error" data-error="new_password_repeat">
        <small></small>
    </div>
    <div class="row text-center">
        <input class="btn btn-order" type="submit" value="Войти">
    </div>
</form>