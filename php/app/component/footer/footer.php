<footer id="footer">
    <div class="container">
        <p class="copyright text-center">© 2015 UIweb <a href="<?=$this->url->home()?>" target="_blank"></a></p>
    </div>
    <div class="container-custom">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <a class="icon vk" onclick="core.popup.open(event, this.href, 'Вконтакте', 800, 800, false); return false;" href="https://vk.com/share.php?url=https://vk.com/uiweb_ru&title=UIweb.ru&description=UIweb.ru осуществляет разработку веб-сайтов с использованием современных CMS и фреймворков&image=<?=$this->url->home()?>/php/assets/common/img/logo.svg" target="_po"></a>
            <a class="icon ok" onclick="core.popup.open(event, this.href, 'Вконтакте', 800, 800, false); return false;" href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st.comments=UIweb.ru осуществляет разработку веб-сайтов с использованием современных CMS и фреймворков&st._surl=<?=$this->url->home()?>"></a>
            <a class="icon fb" onclick="core.popup.open(event, this.href, 'Facebook', 800, 800, false); return false;" href="http://www.facebook.com/sharer.php?s=100&p[url]=<?=$this->url->home()?>&p[title]=UIweb.ru&p[summary]=UIweb.ru осуществляет разработку веб-сайтов с использованием современных CMS и фреймворков&p[images][0]=<?=$this->url->home()?>/php/assets/common/img/logo.svg"></a>
            <a class="icon gp" onclick="" href=""></a>
            <a class="icon tw" onclick="core.popup.open(event, this.href, 'Facebook', 800, 800, false); return false;" href="http://twitter.com/share?text=UIweb.ru осуществляет разработку веб-сайтов с использованием современных CMS и фреймворков&url=<?=$this->url->home()?>"></a>
            <a class="icon pi" onclick="" href="https://www.pinterest.com/pin/create/button/?url=<?=$this->url->home()?>&media=<?=$this->url->home()?>/php/assets/common/img/logo.svg&description=UIweb.ru осуществляет разработку веб-сайтов с использованием современных CMS и фреймворков"></a>
        </div>
        <!--<a href=""><i class="fa fa-pinterest"></i></a>
        <a href=""><i class="fa fa-linkedin"></i></a>
        <a href=""><i class="fa fa-skype"></i></a>-->
    </div>
</footer>