<div id="form-feedback-block" class="page">
	<form id="form-feedback" action="<?=$this->url->route('ajax', 'form-feedback')?>" method="post" autocomplete="off">
		<h1 class="text-center">Готовы сделать заказ?</h1>
		<h2 class="text-center">Заполните форму и мы свяжемся с Вами</h2>
		<div class="alert alert-success" id="success">
			<small></small>
		</div>
		<div class="alert alert-error" id="error">
			<small></small>
        </div>
        <div class="input-container">
            <input type="text" name="name" id="name" placeholder="Как к вам можно обращаться">
            <img data-clear-input="name" class="input-clear" src="/php/assets/component/modal/img/close.png">
        </div>
        <div class="alert alert-error" data-error="name">
			<small></small>
		</div>
        <div class="input-container">
            <input type="text" name="email" id="email" placeholder="Адрес электронной почты">
            <img data-clear-input="email" class="input-clear" src="/php/assets/component/modal/img/close.png">
        </div>
        <div class="alert alert-error" data-error="email">
			<small></small>
		</div>

        <div class="input-container">
            <!-- Dropdown menu start -->

            <div id="phone-code" data-country-id="1" data-country-pattern="\(\d\d\d\)\d\d\d-\d\d-\d\d">
                <div class="country-flag" style="background-position: 0 -660px"></div>
                <span class="country-code">+7</span>
                <div class="clearfix"></div>
            </div>
            <div id="phone-code-container">
                <div id="phone-code-search-container">
                    <input autocomplete="off" type="text" name="q" id="country-search" placeholder="Поиск">
                </div>
                <div id="phone-countries-container">
                    <? foreach($countries as $country){ ?>
                        <div class="phone-container" data-country-id="<?=$country->id?>" data-country-name="<?=$country->name?>" data-country-alias="<?=$country->alias?>" data-country-pattern="<?=$country->pattern?>" data-country-code="<?=$country->code?>" data-country-flag="<?=$country->flag?>">
                            <div class="country-flag" style="background-position: 0 <?=$country->flag?>px"></div>
                            <span class="country-name"><?=$country->name?></span>
                            <span class="country-code"><?=$country->code?></span>
                            <div class="clearfix"></div>
                        </div>
                    <? } ?>
                </div>
            </div>

            <!-- Dropdown menu end -->

            <input type="text" name="phone" id="phone" placeholder="Телефон" autocomplete="off">
            <img data-clear-input="phone" class="input-clear" src="/php/assets/component/modal/img/close.png">
        </div>
        <div class="clearfix"></div>
		<div class="alert alert-error" data-error="phone">
			<small></small>
		</div>
		<h2>Что вас интересует?</h2>
		<label for="website">
			<span class="one-line">Веб-сайт</span>
		</label>
		<input type="checkbox" id="website" name="topic[]" value="website">
		<label for="apps">
			<span class="two-line">Мобильные приложения</span>
		</label>
		<input type="checkbox" id="apps" name="topic[]" value="apps">
		<label for="seo">
			<span class="one-line">SEO</span>
		</label>
		<input type="checkbox" id="seo" name="topic[]" value="seo">
		<textarea name="text" id="text" rows="7" data-min-rows="7" placeholder="Напишите о своём проекте"></textarea>
		<div class="row text-center">
			<input class="btn btn-order" type="submit" value="Сделать заказ">
		</div>
	</form>
</div>