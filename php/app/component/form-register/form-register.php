<form id="form-register" method="post" action="<?=$this->url->route('ajax', 'form-register')?>">
    <h1 class="text-center">Регистрация</h1>
    <h2 class="text-center">Заполните форму</h2>
    <div class="alert alert-success" id="success">
        <small></small>
    </div>
    <div class="alert alert-error" id="error">
        <small></small>
    </div>
    <div class="input-container">
        <input id="username" type="text" name="username" placeholder="Имя пользователя">
        <img data-clear-input="username" class="input-clear" src="/php/assets/component/modal/img/close.png">
    </div>
    <div class="alert alert-error" data-error="username">
        <small></small>
    </div>
    <div class="input-container">
        <input id="email" type="text" name="email" placeholder="Адрес электронной почты">
        <img data-clear-input="email" class="input-clear" src="/php/assets/component/modal/img/close.png">
    </div>
    <div class="alert alert-error" data-error="email">
        <small></small>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="input-container">
            <input id="password" type="password" name="password" placeholder="Пароль">
            <img data-show-input="password" data-show-input-opened="/php/assets/common/img/closed_eye.png" data-show-input-closed="/php/assets/common/img/eye.png" class="show-password left" src="/php/assets/common/img/eye.png">
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="input-container">
            <input id="password_repeat" type="password" name="password_repeat" placeholder="Подтверждение пароля">
            <img data-show-input="password_repeat" data-show-input-opened="/php/assets/common/img/closed_eye.png" data-show-input-closed="/php/assets/common/img/eye.png" class="show-password right" src="/php/assets/common/img/eye.png">
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="alert alert-error" data-error="password">
        <small></small>
    </div>
    <div class="alert alert-error" data-error="password_repeat">
        <small></small>
    </div>
    <div class="row text-center">
        <input class="btn btn-order" type="submit" value="Зарегистрироваться">
    </div>
</form>