<div id="carousel" class="page">
    <h1 class="text-center">Наши проекты</h1>
    <div>
        <div>
            <div class="tablet-glass"></div>
            <div class="tablet-border-shadow"></div>
            <div class="tablet-slider">
                <div class="slider-inner">
                    <? $k = 0; ?>
                    <? foreach($portfolio_projects as $project){ ?>
                        <div id="slide-<?=$k?>">
                            <img src="<?=$project->img_path?>" alt="<?=$project->name?>" title="<?=$project->name?>">
                            <div class="slider-bottom-link" data-dropdown="" data-dropdown-direction="bottom" data-dropdown-selector="slide-<?=$k?>">
                                <a href="#" onclick="core.modal.get(
                                    event,
                                    {
                                        url: '<?=$this->url->route('portfolio', 'view', $project->id)?>',
                                        title: '<?=$project->name?>',
                                        type: 'JSON',
                                        success: function(response, e){
                                            core.preloader.show();

                                            var image = new Image();
                                            image.src = response.url;

                                            image.onload = function() {
                                                core.preloader.hide(function(){
                                                    core.modal.open(e, '<?=$project->name?>');
                                                    core.modal.inner.innerHTML = '<img style=\'width: 100%\' src=\'' + response.url + '\'>';
                                                });
                                            };

                                            //core.modal.open(e, '<?=$project->name?>');
                                            //core.modal.inner.innerHTML = '<img style=\'width: 100%\' src=\'' + response.url + '\'>';
                                            //console.log(core.modal);
                                        },
                                        error: function(response, e) {

                                        },
                                        onloadstart:function(e, request) {
                                            core.preloader.show();
                                        }
                                    })">Подробнее о сайте <?=$project->name?></a>
                            </div>
                        </div>
                        <?$k++;?>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
    <ul class="slider-navigation">
    </ul>
</div>