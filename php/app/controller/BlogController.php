<?
namespace app\controller;

use app\asset\InnerAssetBundle;
use app\layer\LayerController;
use app\model\Note;
use app\model\NoteCategory;
use app\model\NotesTag;
use app\model\Tag;
use app\system\ErrorController;

class BlogController extends LayerController
{
    public function __construct()
    {
        parent::__construct();
        $this->core->asset = new InnerAssetBundle();
        $this->core->asset->addHeaderCss('/php/assets/inner/css/blog.css');
    }

    public function index()
    {
        $this->core->layout = 'inner';
        $tags = (new Tag)
            ->query
            ->select()
            ->where('`tags`.`is_published` = "1"')
            ->execute()
            ->all()
            ->getResult();

        $categories = (new NoteCategory())
            ->query
            ->select()
            ->where('`note-categories`.`is_published` = "1"')
            ->order('created_at')
            ->execute()
            ->all('index')
            ->getResult();

        //вывод без параметров
        if(isset($_GET['category'])){
            $categoty = (new NoteCategory())
                ->query
                ->select()
                ->where('`note-categories`.`is_published` = "1" AND `note-categories`.`alias` = :alias', [':alias' => $_GET['category']])
                ->limit()
                ->execute()
                ->one('index')
                ->getResult();

            if(isset($categoty)){
                $this->core->description = 'UIweb.ru блог статьи по тегу ' . $categoty->name;
                $this->core->keywords = $categoty->name;
                $this->core->title = 'UIweb.ru блог статьи по тегу ' . $categoty->name;

                $notes = (new Note)
                    ->query
                    ->select()
                    ->where('`notes`.`is_published` = "1" AND `notes`.`category_id` = :category_id', [':category_id' => $categoty->id])
                    ->order('created_at')
                    ->execute()
                    ->all('index')
                    ->getResult();
            }else{
                $error = '404';
                die((new ErrorController())->$error());
            }
        }else if(isset($_GET['tag'])){
            $tag = (new Tag())
                ->query
                ->select()
                ->where('`tags`.`is_published` = "1" AND `tags`.`alias` = :alias', [':alias' => $_GET['tag']])
                ->limit()
                ->execute()
                ->one('index')
                ->getResult();

            if(isset($tag)){
                $this->core->description = 'UIweb.ru блог статьи по тегу ' . $tag->name;
                $this->core->keywords = $tag->name;
                $this->core->title = 'UIweb.ru блог статьи по тегу ' . $tag->name;

                $notes = (new Note)
                    ->query
                    ->select()
                    ->where('`notes`.`is_published` = "1" AND `notes-tags`.`tag_id` = :tag_id', [':tag_id' => $tag->id])
                    ->order('created_at')
                    ->execute()
                    ->all('index')
                    ->getResult();
            }else{
                $error = '404';
                die((new ErrorController())->$error());
            }
        }else{
            $this->core->description = 'UIweb.ru блог';
            $this->core->keywords = 'uiweb программист дизайнер блог';
            $this->core->title = 'UIweb - блог';

            $notes = (new Note)
                ->query
                ->select()
                ->where('`notes`.`is_published` = "1"')
                ->order('created_at')
                ->execute()
                ->all('index')
                ->getResult();
        }

        return $this->render(
            [
                'notes' => $notes,
                'tags' => $tags,
                'categories' => $categories
            ]
        );
    }

    public function view($alias)
    {
        $this->core->layout = 'inner';
        $note = (new Note())
            ->query
            ->select()
            ->where('`notes`.`alias` = :alias', [':alias' => $alias])
            ->limit()
            ->execute()
            ->one()
            ->getResult();

        if(isset($note)){
            $this->core->title = empty($note->meta_title) ? $note->name : $note->meta_title;
            $this->core->keywords = empty($note->mata_keywords) ? $note->name : $note->mata_keywords;
            $this->core->description = empty($note->mata_keywords) ? $note->name : $note->mata_keywords;

            $tags = (new Tag)
                ->query
                ->select()
                ->where('`tags`.`is_published` = "1"')
                ->execute()
                ->all()
                ->getResult();


            $categories = (new NoteCategory())
                ->query
                ->select()
                ->where('`note-categories`.`is_published` = "1"')
                ->order('created_at')
                ->execute()
                ->all('index')
                ->getResult();

            $note_tags = (new NotesTag('tags'))
                ->query
                ->select()
                ->where('`tags`.`is_published` = "1"')
                ->execute()
                ->all()
                ->getResult();

            return $this->render(
                [
                    'note' => $note,
                    'note_tags' => $note_tags,
                    'tags' => $tags,
                    'categories' => $categories
                ]
            );
        }else{
            $error = '404';
            die((new ErrorController())->$error());
        }
    }

    public function create()
    {
        $this->core->asset->addHeaderCss('/php/assets/inner/css/create.css');

        $this->core->layout = 'inner';
        if ($_POST['mode'] == 'create') {
            (new Note())->load($_POST, 'insert')->insert();
        }

        return $this->render();
    }

    public function edit($id)
    {
        $this->core->asset->addHeaderCss('/php/assets/inner/css/create.css');

        $this->core->layout = 'inner';
        $note = (new Note())
            ->query
            ->select()
            ->where('`notes`.`id` = :id', [':id' => $id])
            ->limit()
            ->execute()
            ->one()
            ->getResult();

        if(isset($note)){
            if ($_POST['mode'] == 'edit') {
                (new Note())->load($_POST, 'insert')->insert();
            }

            return $this->render(
                [
                    'note' => $note
                ]
            );
        }else{
            $error = '404';
            die((new ErrorController())->$error());
        }
    }
}