<?
namespace app\controller;

use app\asset\FormAssetBundle;
use app\layer\LayerController;
use app\model\User;
use core\Auth;
use core\helper\Flash;

class UserController extends LayerController
{
    public function index()
    {
        //$mail = (new Mail())->send('olegstan@inbox.ru', 'Регистрация', 'register');

        /*$this->core->layout = 'inner';
        $this->core->asset = new InnerAssetBundle();

        $users = (new User())->findAll();
        return $this->render($users);*/
    }

    public function view($id)
    {
        $this->core->layout = 'inner';
        $this->core->asset = new InnerAssetBundle();
        $user = (new User())->findById($id);
        return $this->render(
            [
                'user' => $user
            ]
        );
    }

    public function register()
    {
        $this->core->asset = new FormAssetBundle();
        $this->core->asset->addAsset('headerCSS', '/php/assets/component/form-register/css/form-register.css');

        $this->core->description = 'UIweb.ru регистрация';
        $this->core->keywords = 'регистрация';
        $this->core->title = 'UIweb - регистрация';

        if ($this->core->auth->is_auth) {
            $this->core->flash->set('auth', 'Вы уже авторизовались');
            $this->redirect->redirect();
        } else {
            return $this->render();
        }
    }

    public function login()
    {
        $this->core->asset = new FormAssetBundle();
        $this->core->asset->addAsset('headerCSS', '/php/assets/component/form-login/css/form-login.css');

        $this->core->description = 'UIweb.ru авторизация';
        $this->core->keywords = 'вход на сайт авторизация';
        $this->core->title = 'UIweb - авторизация';

        if ($this->core->auth->is_auth) {
            $this->core->flash->set('auth', 'Вы уже авторизовались');
            $this->redirect->redirect();
        } else {
            return $this->render();
        }
    }

    public function logout()
    {
        $this->core->auth->logout();

        $this->core->auth = new Auth();
        $this->core->flash = new Flash();
        $this->core->flash->set('auth', 'Вы успешно вышли из своего аккаунта');

        return $this->redirect->redirect();
    }

    public function changePassword()
    {
        $this->core->asset = new FormAssetBundle();
        $this->core->asset->addAsset('headerCSS', '/php/assets/component/form-change-password/css/form-change-password.css');

        $this->core->description = 'UIweb.ru смена пароля';
        $this->core->keywords = 'поменять пароль';
        $this->core->title = 'UIweb - смена пароля';

        if ($this->core->auth->is_auth) {
            return $this->render();
        } else {
            $this->core->flash->set('error', 'Вы уже авторизованы');
            return $this->redirect->redirect();
        }
    }

    public function forgetPassword()
    {

    }
}