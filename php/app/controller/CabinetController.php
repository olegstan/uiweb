<?
namespace app\controller;

use app\layer\LayerController;

class CabinetController extends LayerController
{
    public function index()
    {
        if ($this->core->auth->is_auth) {

        }
    }

    public function files()
    {
        if ($this->core->auth->is_auth) {
            $path_to_dir = $_SERVER['DOCUMENT_ROOT'] . '/php/assets/uploads/user/' . $this->core->auth->user->id;

            if (!file_exists($path_to_dir)) {
                if (!mkdir($path_to_dir, 0755)) {
                    die('Не удалось создать папку');
                }
            }

            $files = (new File())->getFiles($path_to_dir);

            if ($files) {
                foreach ($files as $file) {
                    echo $file;
                }
            } else {
                die('Нет файликов');
            }
        }
    }
}