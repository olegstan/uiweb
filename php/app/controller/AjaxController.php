<?
namespace app\controller;

use app\layer\LayerController;
use app\model\FormBackcall;
use app\model\FormFeedback;
use app\model\User;
use core\Response;

class AjaxController extends LayerController
{
    public function __construct()
    {
        parent::__construct();
        $this->core->layout = 'ajax';
    }

    public function formRegister()
    {
        Response::json((new User())->load($_POST, 'register')->register());
    }

    public function formLogin()
    {
        Response::json((new User())->load($_POST, 'login')->login());
    }

    public function formFeedback()
    {
        Response::json((new FormFeedback())->load($_POST, 'insert')->insert());
    }

    public function formBackcall()
    {
        Response::json((new FormBackcall())->load($_POST, 'insert')->insert());
    }

    public function formChangePassword()
    {
        Response::json((new User())->load($_POST, 'change-password')->changePassword());
    }

    public function project()
    {
        //die((new Project())->ajaxMethod($_POST));
    }

    public function projectPassword()
    {
        //die((new ProjectPassword())->ajaxMethod($_POST));
    }

    public function good()
    {
        return $this->render();
    }
}