<?
namespace app\controller;

use app\layer\LayerController;
use app\model\Category;
use app\model\Note;
use app\model\Tag;
use core\helper\File;
use SimpleXMLElement;


class ConfigController extends LayerController
{
    public function index()
    {
        echo (version_compare(PHP_VERSION, '5.5.0') >= 0) ? '<span style="color: green">версия php ' . PHP_VERSION . '</span><br>' : '<span style="color: red">требуется версия 5.5.0</span><br>';
        //mbstring
        echo extension_loaded('curl') ? '<span style="color: green">curl установлен</span><br>' : '<span style="color: red">curl не установлен</span><br>';
        echo extension_loaded('json') ? '<span style="color: green">json установлен</span><br>' : '<span style="color: red">json не установлен</span><br>';
        echo extension_loaded('gd') ? '<span style="color: green">gd установлен</span><br>' : '<span style="color: red">gd не установлен</span><br>';
        echo extension_loaded('sockets') ? '<span style="color: green">sockets установлен</span><br>' : '<span style="color: red">sockets не установлен</span><br>';
        echo extension_loaded('openssl') ? '<span style="color: green">openssl установлен</span><br>' : '<span style="color: red">openssl не установлен</span><br>';
        echo extension_loaded('pdo') ? '<span style="color: green">pdo установлен</span><br>' : '<span style="color: red">pdo не установлен</span><br>';
        echo extension_loaded('mbstring') ? '<span style="color: green">mbstring установлен</span><br>' : '<span style="color: red">mbstring не установлен</span><br>';
        echo extension_loaded('iconv') ? '<span style="color: green">iconv установлен</span><br>' : '<span style="color: red">iconv не установлен</span><br>';
        echo extension_loaded('mcrypt') ? '<span style="color: green">mcrypt установлен</span><br>' : '<span style="color: red">mcrypt не установлен</span><br>';
        echo extension_loaded('redis') ? '<span style="color: green">redis установлен</span><br>' : '<span style="color: red">redis не установлен</span><br>';

        $current_language_key = array_search($this->core->language, $this->core->languages);
        $current_locale = $this->core->locales[$current_language_key];
        $set_locale_res = setlocale(LC_ALL, $current_locale . '.UTF8');

        echo $set_locale_res !== false ? '<span style="color: green">локаль установлена коректно ' . $current_locale . '.UTF8' . '</span><br>' : '<span style="color: red">локаль не установлена ' . $current_locale . '.UTF8' . '</span><br>';
    }

    public function sitemap()
    {
        $urlset = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset></urlset>');

        $urlset->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

        $date = date('c', time());

        $url = $urlset->addChild('url');
        $url->addChild('loc', SERVER_PROTOCOL . '://uiweb.ru');
        $url->addChild('lastmod', $date);

        $url = $urlset->addChild('url');
        $url->addChild('loc', SERVER_PROTOCOL . '://uiweb.ru/main/services');
        $url->addChild('lastmod', $date);

        $url = $urlset->addChild('url');
        $url->addChild('loc', SERVER_PROTOCOL . '://uiweb.ru/main/contacts');
        $url->addChild('lastmod', $date);

        $url = $urlset->addChild('url');
        $url->addChild('loc', SERVER_PROTOCOL . '://uiweb.ru/main/blog');
        $url->addChild('lastmod', $date);

        $blog_tags = (new Tag())
            ->query
            ->select(['id', 'alias'])
            ->execute()
            ->all()
            ->getResult();

        foreach($blog_tags as $tag){
            $url = $urlset->addChild('url');
            $url->addChild('loc', SERVER_PROTOCOL . '://uiweb.ru/blog/?tag=' . $tag->alias);
            $url->addChild('lastmod', $date);
        }

        $blog_category = (new Category())
            ->query
            ->select(['id', 'alias'])
            ->execute()
            ->all()
            ->getResult();

        foreach($blog_category as $category){
            $url = $urlset->addChild('url');
            $url->addChild('loc', SERVER_PROTOCOL . '://uiweb.ru/blog/?category=' . $category->alias);
            $url->addChild('lastmod', $date);
        }

        $notes = (new Note)
            ->query
            ->select()
            ->where('`notes`.`is_published` = "1"')
            ->order('created_at')
            ->execute()
            ->all()
            ->getResult();

        foreach($notes as $note){
            $url = $urlset->addChild('url');
            $url->addChild('loc', SERVER_PROTOCOL . '://uiweb.ru/view/' . $note->alias);
            $url->addChild('lastmod', date('c', $note->created_at));
        }

        $res = $urlset->asXML();

        (new File)->saveAsString($res, 'sitemap.xml', ABS . '/');
    }
}