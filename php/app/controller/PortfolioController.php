<?
namespace app\controller;

use app\layer\LayerController;
use app\model\PortfolioProject;
use core\Response;

class PortfolioController extends LayerController
{
    public function view($id){
        $portfolio_project = (new PortfolioProject())->findById($id)->one()->getResult();

        if($portfolio_project){
            //$file = file_get_contents(ABS . $portfolio_project->portfolio_img_path);
            //echo base64_encode($file);
            //die();
            Response::json(['result' => 'success', 'url' => $portfolio_project->portfolio_img_path]);
        }else{

        }
    }
}