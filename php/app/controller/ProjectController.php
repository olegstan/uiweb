<?
namespace app\controller;

use app\layer\LayerController;

class ProjectController extends LayerController
{
    public function __construct()
    {
        parent::__construct();
        $this->core->headerCSS = [
            '/php/assets/common/css/fonts.css',
            '/php/assets/common/css/normalize.css',
            '/php/assets/common/css/custom-bootstrap.css',
            '/php/assets/common/css/header.css',
            '/php/assets/common/css/footer.css'
        ];
    }

    public function index()
    {
        $this->core->layout = 'inner';

        $this->core->headerCSS[] = '/php/assets/inner/css/inner.css';

        if ($this->core->auth->is_auth) {
            $users_projects = (new UsersProject())->findOwn()->all()->getField('project_id');
            $projects = (new Project())->findById($users_projects)->all()->getResult();
            return $this->render(['projects' => $projects]);
        } else {
            $this->redirect->auth();
        }
    }

    public function view($id)
    {
        $this->core->layout = 'inner';
        if ($this->core->auth->is_auth) {


            $user_projects = (new UsersProject())->query->select()->where('`users-projects`.`user_id` = :user_id && `users-projects`.`project_id` = :project_id', [':user_id' => $this->core->auth->user->id, ':project_id' => $id])->execute()->one()->getResult();

            if ($user_projects !== null) {
                $project = (new Project())->query->select()->where('`projects`.`id` = :id', [':id' => $user_projects->project_id])->execute()->one()->getResult();

                $this->core->title = $project->name;
                $this->core->keywords = $project->name;
                $this->core->description = $project->name;

                $projects_passwords = (new ProjectsPassword())->query->select()->where('`projects-passwors`.`project_id` = :project_id', [':project_id' => $project->id])->execute()->all()->getField('password_id');

                $project_passwords = (new ProjectPassword())->findById($projects_passwords)->all()->getResult();

                return $this->render([
                    'project' => $project,
                    'projectPasswords' => $project_passwords,
                ]);
            } else {
                $this->redirect->redirect();
            }
        } else {
            $this->redirect->auth();
        }
    }

    public function add()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }

}