<?
namespace app\controller;

use app\asset\InnerAssetBundle;
use app\asset\MainAssetBundle;
use app\layer\LayerController;
use app\model\Country;
use app\model\PortfolioProject;

class MainController extends LayerController
{
    public function index()
    {
        /*$res = mail('olegstan1@gmail.com', 'test', 'hello world');

        var_dump($res);*/

        $this->core->layout = 'main';
        $this->core->asset = new MainAssetBundle();

        $this->core->description = 'UIweb.ru осуществляет разработку веб-сайтов с использованием современных CMS и фреймворков';
        $this->core->keywords = 'разработка сайты php javascript html Москва';
        $this->core->title = 'UIweb - разработка веб-сайтов в Москве';

        $countries = (new Country)->query->select()->where('`countries`.`id` != ""')->execute()->all()->getResult();
        $portfolio_projects = (new PortfolioProject)->query->select()->execute()->all(true)->getResult();

        //echo ABS;

        return $this->render(
            [
                'countries' => $countries,
                'portfolio_projects' => $portfolio_projects
            ]
        , false);
        //echo '<!--Final: '.memory_get_usage().' bytes-->';
        //echo '<!--Peak: '. memory_get_peak_usage().' bytes-->';

    }

    public function contacts()
    {
        $this->core->layout = 'inner';
        $this->core->asset = new InnerAssetBundle();
        $this->core->asset->addHeaderCSS('/php/assets/inner/css/contacts.css');
        $this->core->asset->addHeaderCSS('/php/assets/component/form-backcall/css/form-backcall.css');

        $this->core->asset->addFooterJS('/php/assets/common/js/ajaxform.js');
        $this->core->asset->addFooterJS('/php/assets/common/js/dropdown.js');
        $this->core->asset->addFooterJS('/php/assets/common/js/mask.js');
        $this->core->asset->addFooterJS('/php/assets/common/js/keymap.js');
        $this->core->asset->addFooterJS('/php/assets/common/js/customselect.js');

        $this->core->description = 'UIweb.ru Москва контакты';
        $this->core->keywords = 'uiweb программист дизайнер';
        $this->core->title = 'UIweb - контакты в Москве';

        $countries = (new Country)->query->select()->where('`countries`.`id` != ""')->execute()->all()->getResult();

        return $this->render(
            [
                'countries' => $countries
            ],
        false);
    }

    public function services()
    {
        $this->core->layout = 'inner';

        $this->core->asset = new InnerAssetBundle();
        $this->core->asset->addHeaderCSS('/php/assets/inner/css/services.css');

        $this->core->description = 'UIweb.ru Москва услуги';
        $this->core->keywords = 'парсер сайт cms bitrix umi wordpress Москва';
        $this->core->title = 'UIweb - услуги в Москве';

        return $this->render(null, false);
    }

    public function typography()
    {
        $this->core->layout = 'typography';
        $this->core->description = 'UIweb.ru типографика';
        $this->core->keywords = 'типографика';
        $this->core->title = 'UIweb - типографика';

        return $this->render();
    }


    private $client_id = '4764214';
    private $client_secret = 'OUNi0k7me7HwpthXgWxI';

    public function test1()
    {
        $this->core->layout = 'typography';
        return $this->render();
    }
}