<script type="text/javascript" src="/php/assets/common/js/worldmap.js"></script>

<script>
    function VectorImage(options) {
        var self = this;

        self.elem = options.elem;
        self.map = options.map;

        var svg = "http://www.w3.org/2000/svg";
        var group = document.createElementNS(svg, "g");
        self.elem.appendChild(group);

        var path;
        for(var index in self.map){
            path = document.createElementNS(svg, "path");
            path.setAttributeNS(null, "d", self.map[index].path);
            path.setAttributeNS(null, "class", "country");
            path.setAttributeNS(null, "stroke-width", "3");
            path.setAttributeNS(null, "fill", "#" + self.map[index].color);
            path.setAttributeNS(null, "cursor", "pointer");
            path.setAttributeNS(null, "data-country-code", index);
            path.setAttributeNS(null, "data-country-name", self.map[index].name);

            path.addEventListener("mouseover", function(e){
                self.showTooltip(e);
            });

            path.addEventListener("mouseout", function(e){
                self.hideTooltip(e);
            });

            path.addEventListener("mousemove", function(e){
                self.pick(e);
            });

            group.appendChild(path);


        }

        self.tooltip = document.createElementNS(svg, "text");
        self.tooltip.setAttributeNS(null, "id", "tooltip");
        self.tooltip.setAttributeNS(null, "x", 0);
        self.tooltip.setAttributeNS(null, "y", 0);
        self.tooltip.setAttributeNS(null, "cursor", "pointer");
        self.tooltip.setAttributeNS(null, "fill", "#000000");
        self.elem.appendChild(self.tooltip);

        self.init();
    }

    VectorImage.prototype.init = function () {

    };

    VectorImage.prototype.pick = function (event) {
        var self = this;

        self.tooltip.setAttributeNS(null, "x", event.offsetX);
        self.tooltip.setAttributeNS(null, "y", event.offsetY);
    };

    VectorImage.prototype.showTooltip = function(event)
    {
        var self = this;
        var elem = event.target;

        self.tooltip.setAttributeNS(null, "visibility", "visible");

        elem.setAttributeNS(null, "opacity", "0.5");
        self.tooltip.innerHTML = elem.getAttribute('data-country-name');
    };

    VectorImage.prototype.hideTooltip  = function()
    {
        var self = this;
        var elem = event.target;

        self.tooltip.setAttributeNS(null, "visibility", "hidden");

        elem.setAttributeNS(null, "opacity", "1");
    };
</script>

<svg id="map" width="1000px" height="1000px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000">
    <script>
        <![CDATA[
            document.addEventListener("DOMContentLoaded", function () {
                var vector_world_map = new VectorImage({
                    elem: document.getElementById("map"),
                    map: world_map
                });
            });
        ]]>
    </script>
</svg>