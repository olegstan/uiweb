<div id="contacts">
    <div class="">
        <h1>Команда Uiweb</h1>
        <div class="row">
            <div class="col-lg-offset-1 col-lg-3">
                <img class="photo" src="/php/assets/common/img/cooperator0.jpg" alt="uiweb.ru" title="Аборамов Олег">
            </div>
            <div class="col-lg-offset-1 col-lg-6">
                <h2>Абрамов Олег</h2>
                <h3>Программист</h3>
                <h3>Высшее образование МГГУ по специальности "Автоматизация и управление в технических системах"</h3>
                <h3>Более 2 лет опыта профессональной разрабооки веб приложений</h3>
                <h3>Сертифицированный разработчик</h3>
                <h4>телефон: 8(964)789-01-25</h4>
                <h4>также можно связаться через WhatsApp или  Viber</h4>
                <h4>email: olegstan@inbox.ru</h4>
                <h4>skype: <a href="skype:olegstan1?call">olegstan1</a></h4>
                <h4>github: <a href="https://github.com/olegstan" target="_blank">github.com/olegstan</a> </h4>
                <h4>vk: <a href="https://vk.com/olegstan1" target="_blank">olegstan1</a></h4>
                <h4>город: Москва</a></h4>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-lg-offset-1 col-lg-6">
                <h2>Мордовский Андрей</h2>
                <h3>Дизайнер</h3>
                <h4>телефон: 8(916)839-33-76</h4>
                <h4>vk: <a href="https://vk.com/andrey_mordovsky" target="_blank">andrey_mordovsky</a></h4>
            </div>
            <div class="col-lg-offset-1 col-lg-3">
                <img class="photo" src="/php/assets/common/img/cooperator1.jpg" alt="uiweb.ru" title="Мордовский Андрей">
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>



<!-- feedback form start -->
<?=$this->core->view->component->render('form-backcall') ?>
<!-- feedback form end -->

<?=$this->core->asset->footerJS()?>

<script>
    var core;

    document.addEventListener('DOMContentLoaded', function () {
        core = new Core();

        core.scroll = new Scroll();

        core.dropdown.push(
            new Dropdown({
                elem: document.querySelectorAll('[data-dropdown]')
            })
        );

        core.mask.push(
            new Mask({
                elem: document.getElementById('phone'),
                type: 'dynamic',
                mask: function(){
                    return document.getElementById('phone-code').getAttribute('data-country-pattern');
                }
            })
        );

        core.custom_select.push(
            new CustomSelect(
                {
                    search: document.getElementById('country-search'),
                    elem: document.getElementById('phone-code'),
                    container: document.getElementById('phone-code-container'),
                    items: document.querySelectorAll('#phone-countries-container .phone-container'),
                    success_select: function(){
                        //реинит маски
                        core.mask[0].reinit();
                    }
                }
            )
        );

        core.ajax_form.push(
            new AjaxFrom({
                id: "form-backcall",
                empty:{

                },
                fields: [
                    'name',
                    'phone'
                ],
                additionalFunctions: {
                    country_id: function () {
                        return document.getElementById('phone-code').getAttribute('data-country-id')
                    }
                },
                errors_notice: {
                    errorText: function(input, form, getDataJson){
                        input.className = "";
                        input.classList.add('error');
                        var error_cheat=form.querySelector('div[data-error="' + input.name + '"]');
                        error_cheat.style.display='block';
                        error_cheat.firstElementChild.textContent=getDataJson.errors[input.name];
                    }
                },
                success_notice: {
                    successText: function(input, form, getDataJson){
                        input.className = "";
                        input.classList.add('success');
                        var error_cheat=form.querySelector('div[data-error="' + input.name + '"]');
                        error_cheat.style.display='none';
                    }
                },
                success: {
                    successText: function(input, form, getDataJson){
                        input.className = "";
                        input.value = "";
                        var clear_input = form.querySelector('[data-clear-input="' + input.name + '"]');
                        clear_input.parentNode.removeChild(clear_input);
                    }
                },
                //очистка ошибок
                empty: {
                    emptyText: function(input, form){
                        input.className = "";
                        var error_cheat = form.querySelector('div[data-error="' + input.name + '"]');
                        error_cheat.style.display="none";
                        error_cheat.firstElementChild.textContent="";
                    }
                },
                result: {
                    success: function(success_text, submit_button){
                        var success_block = document.getElementById('success');
                        success_block.style.display = 'block';
                        success_block.querySelector('small').textContent = success_text;
                        submit_button.classList.add('disabled');
                    },
                    error: function (error_text, submit_button) {
                        var error_block = document.getElementById('error');
                        error_block.style.display = 'block';
                        error_block.querySelector('small').textContent = error_text;
                    },
                    empty: function (submit_button) {
                        document.getElementById('error').style.display = 'none';
                    }
                }
            })
        );
    });
</script>