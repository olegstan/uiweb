<!-- register form start -->

<?=$this->core->view->component->render('form-register')?>

<!-- register form start -->
<?=$this->core->asset->footerJS()?>

<script>
    var core;

    document.addEventListener("DOMContentLoaded", function () {
        core = new Core();

        core.scroll = new Scroll();

        core.clear_input = new ClearInput();

        core.show_password = new ShowPassword();

        core.ajax_form.push(
            new AjaxFrom({
                id: "form-register",
                fields: [
                    'username',
                    'email',
                    'password',
                    'password_repeat'
                ],
                errors_notice:{
                    errorText: function(input, form, getDataJson){
                        input.className = "";
                        input.classList.add('error');
                        var errorCheat = form.querySelector('div[data-error="' + input.name + '"]');
                        errorCheat.style.display = 'block';
                        errorCheat.firstElementChild.textContent = getDataJson.errors[input.name];
                    },
                    errorPassword: function(input, form, getDataJson){
                        input.className = "";
                        input.classList.add('error');
                        var errorCheat = form.querySelector('div[data-error="' + input.name + '"]');
                        errorCheat.style.display = 'block';
                        errorCheat.firstElementChild.textContent = getDataJson.errors[input.name];
                    }
                },
                success_notice:{
                    successText: function(input, form, getDataJson){
                        input.className = "";
                        input.classList.add('success');
                    },
                    successPassword: function(input, form, getDataJson){
                        input.className = "";
                        input.classList.add('success');
                    }
                },
                success:{
                    successText: function(input, form, getDataJson){
                        input.className = "";
                        input.value = "";
                    },
                    successPassword: function(input, form, getDataJson){
                        input.className = "";
                        input.value = "";
                    }
                },
                //очистка ошибок
                empty:{
                    emptyText: function(input, form){
                        input.className = "";
                        input.classList.remove('error');
                        var errorCheat = form.querySelector('div[data-error="' + input.name + '"]');
                        errorCheat.style.display = 'none';
                        errorCheat.firstElementChild.textContent = '';
                    },
                    emptyPassword: function(input, form){
                        input.className = "";
                        input.classList.remove('error');
                        var errorCheat = form.querySelector('div[data-error="' + input.name + '"]');
                        errorCheat.style.display = 'none';
                        errorCheat.firstElementChild.textContent = '';
                    }
                },
                result: {
                    success: function(success_text, submit_button){
                        var success_block = document.getElementById('success');
                        success_block.style.display = 'block';
                        success_block.querySelector('small').textContent = success_text;
                        submit_button.classList.add('disabled');

                        setTimeout(function(){
                            window.location.replace(core.host);
                        }, 2000);
                    },
                    error: function (error_text, submit_button) {
                        var error_block = document.getElementById('error');
                        error_block.style.display = 'block';
                        error_block.querySelector('small').textContent = error_text;
                    },
                    empty: function (submit_button) {
                        document.getElementById('error').style.display = 'none';
                    }
                }
            })
        );
    }, true);
</script>