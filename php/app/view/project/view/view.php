<div id="form" class="container">
    <div class="row">
        <div class="header">
            <div>id</div>
            <div>name</div>
            <div>url</div>
            <div>description</div>
            <div>login</div>
            <div>password</div>
        </div>
        <div class="clearfix"></div>
        <div class="body" data-project-id="<?=$project->id?>">
            <? if($projectPasswords){ ?>
                <? foreach($projectPasswords as $password){ ?>
                    <form data-form-id="<?=$password->id?>" method="post" action="/ajax/project-password">
                        <input name="id" value="<?=$password->id?>" disabled>
                        <input name="name" value="<?=$password->name?>">
                        <input name="url" value="<?=$password->url?>">
                        <input name="description" value="<?=$password->description?>">
                        <input name="login" value="<?=$password->login?>">
                        <input name="password" value="<?=$password->password?>">
                        <input data-type="update" type="submit" value="Изменить">
                        <input data-type="delete" type="submit" value="Удалить">
                        <div class="clearfix"></div>
                    </form>
                <? } ?>
            <? } ?>
        </div>
        <div class="new-body" data-project-id="<?=$project->id?>">
            <form method="post" action="/ajax/project-password">
                <input name="name">
                <input name="url">
                <input name="description">
                <input name="login">
                <input name="password">
                <input data-type="insert" type="submit" value="Добавить">
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="/php/assets/common/js/ajaxform.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        new AjaxFrom({
            type: "dynamic",
            fields: {
                id:"text",
                name:"text",
                url:"text",
                description:"text",
                login:"text",
                password:"text"
            },
            buttons: {
                update: "Изменить",
                delete: "Удалить"
            }
        });
    }, true);
</script>
<link rel="stylesheet" type="text/css" href="/php/assets/inner/css/project.css">
