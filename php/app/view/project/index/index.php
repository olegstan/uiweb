<div id="form" class="container">
    <div class="row">
        <div class="header">
            <div>id</div>
            <div>name</div>
        </div>
        <div class="clearfix"></div>
        <div class="body">
            <? if($projects){ ?>
                <? foreach($projects as $project){ ?>
                    <form data-form-id="<?=$project->id?>" method="post" action="/ajax/project">
                        <input name="id" value="<?=$project->id?>" disabled>
                        <input name="name" value="<?=$project->name?>">
                        <input data-type="view" data-view="/project/view/<?=$project->id?>" type="submit" value="Просмотр">
                        <input data-type="update" type="submit" value="Изменить">
                        <input data-type="delete" type="submit" value="Удалить">
                        <div class="clearfix"></div>
                    </form>
                <? } ?>
            <? } ?>
        </div>
        <div class="clearfix"></div>
        <div class="new-body">
            <form method="post" action="/ajax/project">
                <input name="name">
                <input data-type="insert" type="submit" value="Добавить">
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="/php/assets/common/js/ajaxform.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        new AjaxFrom({
            type: "dynamic",
            fields: {
                id:"text",
                name:"text"
            },
            buttons: {
                view: "Просмотр",
                update: "Изменить",
                delete: "Удалить"
            }
        });
    }, true);
</script>
<link rel="stylesheet" type="text/css" href="/php/assets/inner/css/project.css">