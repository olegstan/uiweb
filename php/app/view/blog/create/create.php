<div id="create">
    <div class="container-custom">
        <form method="POST">
            <input type="hidden" name="mode" value="create">

            <div class="form-group">
                <label class="col-lg-2" for="name">Название</label>
                <div class="col-lg-10 form-control">
                    <input type="text" id="name" name="name">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2" for="alias">Alias</label>
                <div class="col-lg-10 form-control">
                    <input type="text" name="alias" id="alias" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2" for="text">Текст</label>
                <div class="col-lg-10 form-control">
                    <textarea name="text"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10 form-control">
                    <button type="submit">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>