<div id="blog-view" class="page">
    <div id="left-part" class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        <? if($note){ ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="post-header">
                    <div class="post-info"></div>
                    <div class="post-date">
                        <span class="post-day"><?=$note->created_day?></span>
                        <br>
                        <span class="post-little-day"><?=$note->created_month?> <?=$note->created_day?></span>
                    </div>
                    <h1><?=$note->name?></h1>
                </div>
                <hr>
                <div class="post-text">
                    <?=$note->text?>
                </div>
                <div class="post-tags">
                    <? if($note_tags){ ?>
                        <? foreach ($note_tags as $tag) { ?>
                            <a href="<?=$this->url->route('blog')?>?tag=<?=$tag->alias?>"><?=$tag->name?></a>
                        <? } ?>
                    <? } ?>
                </div>
            </div>
        <? } ?>
    </div>
    <div id="right-part" class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div id="category">
            <? if($categories){ ?>
                <h1>Категории</h1>
                <ul>
                    <? foreach($categories as $category){ ?>
                        <li><a href="<?=$this->url->route('blog')?>?category=<?=$category->alias?>"><?=$category->name?></a></li>
                    <? } ?>
                </ul>
            <? } ?>
        </div>
        <?/*<div id="authors">
                <h1>Авторы</h1>
                <ul>
                    <li><a>1</a></li>
                    <li><a>2</a></li>
                    <li><a>3</a></li>
                    <li><a>4</a></li>
                </ul>
            </div>*/?>
        <div id="tags">
            <? if($tags){ ?>
                <h1>Теги</h1>
                <ul>
                    <? foreach($tags as $tag){ ?>
                        <li><a href="<?=$this->url->route('blog')?>?tag=<?=$tag->alias?>"><?=$tag->name?></a></li>
                    <? } ?>
                </ul>
            <? } ?>
        </div>
    </div>
</div>