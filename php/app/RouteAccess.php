<?
namespace app;

use app\system\ErrorController;
use core\Core;

class RouteAccess
{
    public function __construct()
    {
        $this->core = Core::getCore();
    }

    //header('Access-Control-Allow-Origin: *');
    //header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    //header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); // allow certain headers

    public function routes()
    {
        return [
            'GET' => [
                'main' => [
                    'index' => [
                        'name' => 'index'
                    ],
                    'services' => [
                        'name' => 'services'
                    ],
                    'contacts' => [
                        'name' => 'contacts'
                    ],
                    'typography' => [
                        'name' => 'typography'
                    ],
                    'test' => [
                        'name' => 'test'
                    ],
                    'test1' => [
                        'name' => 'test1'
                    ]
                ],
                'blog' => [
                    'index' => [
                        'name' => 'index'
                    ],
                    'create' => [
                        'name' => 'create',
                        'required' => [
                            'auth' => true
                        ]
                    ],
                    'view' => [
                        'name' => 'view',
                        'required' => [
                            'param' => true
                        ]
                    ],
                    'edit' => [
                        'name' => 'view',
                        'required' => [
                            'auth' => true,
                            'param' => true
                        ]
                    ]
                ],
                'user' => [
                    //'index' => 'index',
                    'login' => [
                        'name' => 'login'
                    ],
                    'register' => [
                        'name' => 'register'
                    ],
                    'logout' => [
                        'name' => 'logout',
                        'required' => [
                            'auth' => true
                        ],
                    ],
                    'change-password' => [
                        'name' => 'change-password',
                        'required' => [
                            'auth' => true
                        ],
                    ],
                    'forget-password' => [
                        'name' => 'forget-password',
                        'required' => [
                            'auth' => true
                        ],
                    ]
                ],
                'auth' => [
                    'vk' => [
                        'name' => 'vk'
                    ]
                ],
                'config' => [
                    'index' => [
                        'name' => 'index',
                        'required' => [
                            'auth' => true
                        ]
                    ],
                    'sitemap' => [
                        'name' => 'sitemap',
                        'required' => [
                            'auth' => true
                        ]
                    ]
                ],
                'portfolio' => [
                    'view' => [
                        'name' => 'view',
                        'required' => [
                            'param' => true
                        ]
                    ]
                ],
                'ajax' => [
                    'template' => [
                        'name' => 'template',
                        'required' => [
                            'ajax' => true
                        ]
                    ],
                    'test' => [
                        'name' => 'test',
                        'required' => [
                            'ajax' => true
                        ]
                    ]
                ]
            ],
            'POST' => [
                'ajax' => [
                    'form-feedback' => [
                        'name' => 'form-feedback',
                        'required' => [
                            'refer' => 'https://uiweb.ru/'
                        ]
                    ],
                    'form-backcall' => [
                        'name' => 'form-backcall',
                        'required' => [
                            'refer' => 'https://uiweb.ru/main/contacts'
                        ]
                    ],
                    'form-login' => [
                        'name' => 'form-login'
                    ],
                    'form-register' => [
                        'name' => 'form-register'
                    ],
                    'form-change-password' => [
                        'name' => 'form-change-password'
                    ]
                ],
                'blog' => [
                    'create' => [
                        'name' => 'create',
                        'required' => [
                            'auth' => true
                        ]
                    ],
                    'edit' => [
                        'name' => 'create',
                        'required' => [
                            'auth' => true,
                            'param' => true
                        ]
                    ]
                ]
            ],
            'PUT',
            'PATCH',
            'TRACE',
            'DELETE',
            'HEAD',
            'OPTIONS'
        ];
    }

    public function check()
    {
        $routes = $this->routes();

        if(isset($routes[$this->core->method][$this->core->controller][$this->core->action_uri])){
            $current_action = $routes[$this->core->method][$this->core->controller][$this->core->action_uri];

            //проверка на ajax и обязательность
            if(isset($current_action['required']['ajax'])){
                if(isset($_SERVER['HTTP_DETECT_AJAX']) || isset($_SERVER['HTTP_X_REQUESTED_WITH'])){

                }else{
                    $error = 403;
                    die((new ErrorController())->$error('Роут требует установки ajax хедера'));
                }
            }

            //проверка авторизации пользователя
            if(isset($current_action['required']['auth'])){
                if($this->core->auth->is_auth){

                }else{
                    $error = '401';
                    die((new ErrorController())->$error('Попытка входа неавторизованного пользователя на станицу требующую авторизации'));
                }
            }


            if(isset($current_action['required']['refer'])){
                if($this->core->referer == $current_action['required']['refer']){

                }else{
                    $error = '403';
                    die((new ErrorController())->$error('Попытка обратиться с неправильной страницы'));
                }
            }

            //проверка должны ли передаваться параметры в вызов иетода
            if(isset($current_action['required']['param'])){
                //количество роутов с парамметром
                $count_default_routes = $this->core->language == $this->core->default_language ? 4 : 5;
            }else{
                //количество роутов без парамметром
                $count_default_routes = $this->core->language == $this->core->default_language ? 3 : 4;

            }

            if(count($this->core->routes) > $count_default_routes){
                $error = '404';
                die((new ErrorController())->$error('Допустимое количество роутов ' . $count_default_routes . ', a сейчас их ' . count($this->core->routes)));
            }

        }else{
            $error = '404';
            die((new ErrorController())->$error('Ошибка путей, не найден роут'));
        }
    }

    /*
     rest api

    +-----------+---------------------------+---------+------------------+
    |   Verb    |           Path            | Action  |    Route Name    |
    +-----------+---------------------------+---------+------------------+
    | GET       | /resource                 | index   | resource.index   |
    | GET       | /resource/create          | create  | resource.create  |
    | POST      | /resource                 | store   | resource.store   |
    | GET       | /resource/{resource}      | show    | resource.show    |
    | GET       | /resource/{resource}/edit | edit    | resource.edit    |
    | PUT/PATCH | /resource/{resource}      | update  | resource.update  |
    | DELETE    | /resource/{resource}      | destroy | resource.destroy |
    +-----------+---------------------------+---------+------------------+



     */
}