<!DOCTYPE html>
<html  lang="<?=$this->core->language?>">
<head>
    <meta name="robots" content="index, follow">

    <?=$this->core->view->getDescription()?>

    <?=$this->core->view->getKeywords()?>

    <?=$this->core->view->getTitle()?>

    <?=$this->core->view->getAlternate()?>

    <link rel="shortcut icon" type="image/x-icon" href="/php/assets/icon/favicon.ico">

    <?=$this->core->asset->headerCSS()?>
    <?=$this->core->asset->headerJS()?>


    <script type="text/javascript" src="/php/assets/common/js/scroll.js"></script>

    <script>
        //гугл аналитика
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-57682954-1', 'auto');
        ga('send', 'pageview');

        //гугл аналитика

        //яндекс метрика

        (function (d, w, c) {
	        (w[c] = w[c] || []).push(function() {
		        try {
			        w.yaCounter29515030 = new Ya.Metrika({id:29515030,
				        webvisor:true,
				        clickmap:true,
				        trackLinks:true,
				        accurateTrackBounce:true});
		        } catch(e) { }
	        });

	        var n = d.getElementsByTagName("script")[0],
		        s = d.createElement("script"),
		        f = function () { n.parentNode.insertBefore(s, n); };
	        s.type = "text/javascript";
	        s.async = true;
	        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

	        if (w.opera == "[object Opera]") {
		        d.addEventListener("DOMContentLoaded", f, false);
	        } else { f(); }
        })(document, window, "yandex_metrika_callbacks");

        //яндекс метрика
    </script>
</head>

<body>

    <!-- header start -->

    <?=$this->core->view->component->render('header')?>

    <!-- header end -->

    <div id="wrapper">

        <?=$this->core->view->component->render('flash')?>

        <div class="container-custom">
            <section id="content">

                <?=$this->core->view->view->renderView()?>

            </section>
        </div>
    </div>

    <!-- footer start -->

    <?=$this->core->view->component->render('footer')?>

    <!-- footer end -->

    <?=$this->core->view->component->render('modal')?>

</body>
</html>