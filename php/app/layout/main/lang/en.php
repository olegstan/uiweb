<?
return [
    'development' => 'Developing websites and applications',
    'design' => 'Newest design and unique functional',
    'order' => 'Ready for order',
    'learn_more' => 'Learn more',
    'our_tech' => 'Our technology',
    'our_projects' => 'In our projects we use the latest web technologies'
];