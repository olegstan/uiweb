<!DOCTYPE html>
<html lang="<?=$this->core->language?>">
<head>
	<meta name="robots" content="index, follow">

	<?=$this->core->view->getDescription()?>

	<?=$this->core->view->getKeywords()?>

	<?=$this->core->view->getTitle()?>

	<?=$this->core->view->getAlternate()?>

	<link rel="shortcut icon" type="image/x-icon" href="/php/assets/icon/favicon.ico">

    <?=$this->core->asset->headerCss()?>
    <?=$this->core->asset->headerJS()?>

	<script>
		//гугл аналитика

		(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,"script","//www.google-analytics.com/analytics.js","ga");

		ga("create", "UA-57682954-1", "auto");
		ga("send", "pageview");

		//гугл аналитика

		//яндекс метрика

		(function (d, w, c) {
			(w[c]=w[c] || []).push(function() {
				try {
					w.yaCounter29515030=new Ya.Metrika({id:29515030,
						webvisor:true,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true});
				} catch(e) { }
			});

			var n=d.getElementsByTagName("script")[0],
				s=d.createElement("script"),
				f=function () { n.parentNode.insertBefore(s, n); };
			s.type="text/javascript";
			s.async=true;
			s.src=(d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");

		//яндекс метрика
	</script>
</head>
<body id="body">

<!-- header start -->

<?=$this->core->view->component->render('header')?>

<!-- header end -->

<div id="wrapper">

    <?=$this->core->view->component->render('flash')?>

    <section id="content">
		<div id="main" class="page">
			<div class="container-custom">
				<h1 class="text-center text-uppercase"><?=$t['development']?></h1>
				<h2 class="text-center text-uppercase"><?=$t['design']?></h2>
				<div class="buttons text-center">
					<button class="btn btn-default" data-href="our-tech"><?=$t['learn_more']?></button>
					<button class="btn btn-order" data-href="form-feedback-block"><?=$t['order']?></button>
				</div>
			</div>
		</div>

		<div id="our-tech" class="page">
			<h1 class="text-center"><?=$t['our_tech']?></h1>
			<h4 class="text-center"><?=$t['our_projects']?></h4>
			<div class="container-custom">
				<div class="row">
					<div class="col-lg-offset-1 col-lg-3 tech-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <title>HTML5</title>
                            <text x="140" y="80" style="fill:black;font-weight:bolder;font-size:80px">HTML5</text>
                            <g>
                                <polygon fill="#E44D26" points="107.644,470.877 74.633,100.62 437.367,100.62 404.321,470.819 255.778,512"/>
                                <polygon fill="#F16529" points="256,480.523 376.03,447.246 404.27,130.894 256,130.894"/>
                                <polygon fill="#EBEBEB" points="256,268.217 195.91,268.217 191.76,221.716 256,221.716 256,176.305 255.843,176.305 142.132,176.305 143.219,188.488 154.38,313.627 256,313.627"/>
                                <polygon fill="#EBEBEB" points="256,386.153 255.801,386.206 205.227,372.55 201.994,336.333 177.419,336.333 156.409,336.333 162.771,407.634 255.791,433.457 256,433.399"/>
                                <polygon fill="#FFFFFF" points="255.843,268.217 255.843,313.627 311.761,313.627 306.49,372.521 255.843,386.191 255.843,433.435 348.937,407.634 349.62,399.962 360.291,280.411 361.399,268.217 349.162,268.217"/>
                                <polygon fill="#FFFFFF" points="255.843,176.305 255.843,204.509 255.843,221.605 255.843,221.716 365.385,221.716 365.385,221.716 365.531,221.716 366.442,211.509 368.511,188.488 369.597,176.305"/>
                            </g>
                        </svg>
                    </div>
					<div class="col-lg-offset-1 col-lg-6">
						<h2>HTML5</h2>
						<h4><?=$t['our_sites']?></h4>
						<h4><?=$t['valid']?></h4>
						<h4><?=$t['layout']?></h4>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-lg-offset-1 col-lg-6">
						<h2>CSS3</h2>
						<h4><?=$t['css']?></h4>
						<h4><?=$t['bootstrap']?></h4>
					</div>
                    <? if(!$this->core->auth->is_auth){ ?>
					<div class="col-lg-offset-1 col-lg-3 tech-icon icon-css3">
                    </div>

                    <? }else{ ?>
                        <div class="col-lg-offset-1 col-lg-3 tech-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <title>HTML5</title>
                                <g>
                                    <polygon fill="#0170BA" points="107.644,470.877 74.633,100.62 437.367,100.62 404.321,470.819 255.778,512"/>
                                    <polygon fill="#29A9DF" points="256,480.523 376.03,447.246 404.27,130.894 256,130.894"/>
                                    <polygon fill="#EBEBEB" points="255.843,176.305 255.843,204.509 255.843,221.605 255.843,221.716 129.843,221.716 125.843,176.305"/>


                                    <polygon fill="#FFFFFF" points="255.843,176.305 255.843,204.509 255.843,221.605 255.843,221.716 365.385,221.716 365.385,221.716 365.531,221.716 366.442,211.509 368.511,188.488 369.597,176.305"/>
                                    <polygon fill="#FFFFFF" points="365.385,211.716 335.385,421.716 315.335,221.716"/>
                                </g>
                            </svg>
                        </div>

                    <? } ?>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-lg-offset-1 col-lg-3 tech-icon icon-php"></div>
					<div class="col-lg-offset-1 col-lg-6">
						<h2>PHP</h2>
						<h4><?=$t['php_expirience']?></h4>
						<h4><?=$t['cms']?></h4>
						<h4><?=$t['oop']?></h4>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-lg-offset-1 col-lg-6">
						<h2>Javascript</h2>
						<h4><?=$t['easy_to_use']?></h4>
						<h4><?=$t['javascript']?></h4>
					</div>
					<div class="col-lg-offset-1 col-lg-3 tech-icon icon-javascript"></div>
				</div>
				<div class="row">
					<div class="col-lg-offset-1 col-lg-3 tech-icon icon-angularjs"></div>
					<div class="col-lg-offset-1 col-lg-6">
						<h2>AngularJs</h2>
						<h4><?=$t['angularjs']?></h4>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>

		<!-- carousel start -->
		<?=$this->core->view->component->render('carousel') ?>
		<!-- carousel end -->

		<!-- feedback form start -->
		<?=$this->core->view->component->render('form-feedback') ?>
		<!-- feedback form end -->

	</section>


</div>

<!-- footer start -->

<?=$this->core->view->component->render('footer')?>

<!-- footer end -->

<!-- modal start -->

<?=$this->core->view->component->render('modal')?>

<!-- modal end -->

<!-- preloader start -->

<?=$this->core->view->component->render('preloader')?>

<!-- preloader start -->

<?=$this->core->asset->footerJS()?>

<script>

	var core;

	document.addEventListener('DOMContentLoaded', function () {

		core = new Core();

        core.dropdown.push(
             new Dropdown({
                elem: document.querySelectorAll('[data-dropdown]')
             })
        );
		core.carousel.push(
			new Carousel({
				carousel: document.getElementById('carousel')
			})
		);
		core.smart_form.push(
			new SmartForm({
				elem: document.getElementById('text')
			})
		);
		core.mask.push(
			new Mask({
				elem: document.getElementById('phone'),
				type: 'dynamic',
				mask: function(){
					return document.getElementById('phone-code').getAttribute('data-country-pattern');
				}
			})
		);

		core.custom_select.push(
			new CustomSelect(
				{
					search: document.getElementById('country-search'),
					elem: document.getElementById('phone-code'),
					container: document.getElementById('phone-code-container'),
					items: document.querySelectorAll('#phone-countries-container .phone-container'),
					success_select: function(){
						//реинит маски
						core.mask[0].reinit();
					}
				}
			)
		);

		core.scroll = new Scroll();

        core.clear_input = new ClearInput();

		core.ajax_form.push(
			new AjaxFrom({
				type: "static",
				id: "form-feedback",
				empty:{

				},
                fields: [
                    'name',
                    'email',
                    'phone',
                    'topic[]',
                    'text'
                ],
				additionalFunctions: {
					country_id: function () {
						return document.getElementById('phone-code').getAttribute('data-country-id')
					}
				},
                errors_notice: {
					errorText: function(input, form, getDataJson){
                        input.className = "";
                        input.classList.add('error');
						var error_cheat=form.querySelector('div[data-error="' + input.name + '"]');
                        error_cheat.style.display='block';
                        error_cheat.firstElementChild.textContent=getDataJson.errors[input.name];
					},
					errorTextarea: function(input, form, getDataJson){
                        input.className = "";
						input.classList.add('error');
					},
					errorCheckbox: function(input, form, getDataJson){
						var checkboses=form.querySelectorAll('input[name="' + input.name + '"]');
						Array.prototype.forEach.call(checkboses, function (v, k) {
                            form.querySelector('label[for="' + v.id + '"] span').classList.add('error');
						});
					}
				},
                success_notice: {
                    successText: function(input, form, getDataJson){
                        input.className = "";
                        input.classList.add('success');
                        var error_cheat=form.querySelector('div[data-error="' + input.name + '"]');
                        error_cheat.style.display='none';
                    },
                    successTextarea: function(input, form, getDataJson){
                        input.className = "";
                        input.classList.add('success');
                    },
                    successCheckbox: function(input, form, getDataJson){
                        var checkboses=form.querySelectorAll('input[name="' + input.name + '"]');
                        Array.prototype.forEach.call(checkboses, function (v, k) {
                            form.querySelector('label[for="' + v.id + '"] span').classList.remove('error');
                        });
                    }
                },
				success: {
					successText: function(input, form, getDataJson){
                        input.className = "";
                        input.value = "";
                        var clear_input = form.querySelector('[data-clear-input="' + input.name + '"]');
                        clear_input.parentNode.removeChild(clear_input);
					},
					successTextarea: function(input, form, getDataJson){
                        input.className = "";
                        input.value="";
					},
					successCheckbox: function(input, form){
						form.querySelector('label[for="' + input.id + '"] span').classList.remove('active');
					}
				},
				//очистка ошибок
				empty: {
					emptyText: function(input, form){
                        input.className = "";
                        var error_cheat = form.querySelector('div[data-error="' + input.name + '"]');
                        error_cheat.style.display="none";
                        error_cheat.firstElementChild.textContent="";
					},
					emptyTextarea: function(input, form){
                        input.className = "";
					},
					emptyCheckbox: function(input, form){
                        form.querySelector('label[for="' + input.id + '"] span').classList.remove('error');
					}
				},
                result: {
                    success: function(success_text, submit_button){
                        var success_block = document.getElementById('success');
                        success_block.style.display = 'block';
                        success_block.querySelector('small').textContent = success_text;
                        submit_button.classList.add('disabled');
                    },
                    error: function (error_text, submit_button) {
                        var error_block = document.getElementById('error');
                        error_block.style.display = 'block';
                        error_block.querySelector('small').textContent = error_text;
                    },
                    empty: function (submit_button) {
                        document.getElementById('error').style.display = 'none';
                    }
                }
			})
		);

		var form=document.getElementById("form-feedback");
		//установка чекбоксов
		Array.prototype.forEach.call(form["topic[]"], function (v, k) {
			//after ie10
			v.addEventListener("click", function (e) {
				var id=this.id;
				var checkbox=this.parentNode.querySelectorAll("label[for='" + id + "'] span");
				var input=document.getElementById(id);

				input.checked=!checkbox[0].classList.contains("active");
				checkbox[0].classList.toggle("active");
			});
			v.addEventListener("touch", function (e) {
				var id = this.id;
				var checkbox = this.parentNode.querySelectorAll("label[for='" + id + "'] span");
				var input = form.getElementById(id);

				input.checked=!checkbox[0].classList.contains("active");
				checkbox[0].classList.toggle("active");
			});
		});
	});
</script>
</body>
</html>