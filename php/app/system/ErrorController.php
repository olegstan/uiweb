<?
namespace app\system;

use app\asset\InnerAssetBundle;
use app\layer\LayerController;

class ErrorController extends LayerController
{
    public function __construct()
    {
        parent::__construct();
        $this->core->asset = new InnerAssetBundle;
        $this->core->asset->addHeaderCss('/php/assets/common/css/error.css');

        $this->core->layout = 'inner';
        $this->core->default_layout = 'error';
    }

    public $errors = [
        '401' => [
            'header' => 'Unauthorized'
        ],
        '403' => [
            'header' => 'Forbidden'
        ],
        '404' => [
            'header' => 'Not Found'
        ],
        '500' => [
            'header' => 'Internal Server Error'
        ],
    ];

    public function __call($name, $params){
        $name = isset($this->errors[$name]) ? $name : '500';
        $this->core->action_uri = $name;
        $this->core->title = 'Ошибка ' . $name;

        header('HTTP/1.1 ' . $name . ' ' . $this->errors[$name]['header']);
        header('Status: ' . $name . ' ' . $this->errors[$name]['header']);

        $string = isset($params[0]) ? $params[0] : '';
        echo '<!-- ' . $string . ' -->';
        return $this->render();
    }
}